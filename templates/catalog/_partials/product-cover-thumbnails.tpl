{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
<div class="images-container js-images-container d-flex flex-column-reverse flex-md-row">

{block name='product_images'}
  <div class="js-qv-mask mb-3">
    <nav class="product-images js-qv-product-images d-flex flex-row flex-md-column slick__arrow-outside" data-slick='{literal}{
        "slidesToScroll":1,
        "slidesToShow":9,
        "infinite":false,
        "rows":0,
        "vertical":true,
        "verticalSwiping":true,
        "responsive":[
           {
              "breakpoint":768,
              "settings":{
                 "slidesToShow":3,
                 "vertical":false,
                 "verticalSwiping":false,
                 "variableWidth": true
              }
           }
        ]
     }{/literal}' data-count="{$product.images|count}" data-responsive-height="true">
        {hook h='displayWebrotateThumb' product=$product}
      {foreach from=$product.images item=image}
        <div class="thumb-container js-thumb-container">
          <div class="thumb-container__wrapper{if $image.id_image == $product.default_image.id_image} thumb-container__wrapper--selected{/if}">  
            <img
                class="thumb js-thumb {if $image.id_image == $product.default_image.id_image}js-thumb-selected{/if}"
                data-image-medium-src="{$image.bySize.medium_default.url}"
                data-image-large-src="{$image.bySize.large_default.url}"
                src="{$image.bySize.home_default.url}"
                {if !empty($image.legend)}
                  alt="{$image.legend}"
                  title="{$image.legend}"
                {else}
                  alt="{$product.name}"
                {/if}
                loading="lazy"
                width="94"
                height="94"
                data-img="{$image.id_image}"
              >
            </div>
        </div>
      {/foreach}
    </nav>
  </div>
{/block}

  {block name='product_cover'}

    <div class="product-cover mb-2 mb-md-0">
      {block name='product_flags'}
        {include file='catalog/_partials/product-flags.tpl'}
      {/block}
      {hook h='displayProductWishlist' mod='mwishlist' product=$product}
      {hook h='displayWebrotateBox' product=$product}
      {if $product.default_image}
        <img class="img-fluid product-cover__image lazyload" 
            srcset="{$product.default_image.bySize.medium_default.url} 452w,
                    {$product.default_image.bySize.pdt_180.url} 180w,
                    {$product.default_image.bySize.pdt_300.url} 300w,
                    {$product.default_image.bySize.pdt_360.url} 360w,
                    {$product.default_image.bySize.pdt_540.url} 540w"
            data-src="{$product.default_image.bySize.medium_default.url}" 
            alt="{$product.default_image.legend}"
            title="{$product.default_image.legend}"
            data-img="{$product.default_image.id_image}"
            width="{$product.default_image.bySize.medium_default.width}"
            height="{$product.default_image.bySize.medium_default.height}"
        >
        <div class="wrapper--zoom">
          <a href="{$product.default_image.bySize.large_default.url}" class="btn__zoom hidden-sm-down" data-fancybox="gallery">
            <i class="material-icons zoom-in">search</i>
          </a>        
        </div>
      {else}
        <img 
          src="{$urls.no_picture_image.bySize.large_default.url}"
          loading="lazy"
          width="{$urls.no_picture_image.bySize.large_default.width}"
          height="{$urls.no_picture_image.bySize.large_default.height}"
        >
      {/if}
    </div>
  {/block}

{hook h='displayAfterProductThumbs' product=$product}
</div>