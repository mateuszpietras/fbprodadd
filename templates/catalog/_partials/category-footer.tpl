{if ($category.description || $category.image.large.url) && $listing.pagination.items_shown_from == 1}
    <div class="category__description d-flex">
        {if $category.description}
            <div id="category-description" class="text-muted">{$category.description nofilter}</div>
        {/if}
        {if $category.image.large.url}
            <div class="category-cover">
                <img src="{$category.image.large.url}" class="lazyload" width="{$category.image.large.width}" height="{$category.image.large.height}" alt="{if !empty($category.image.legend)}{$category.image.legend}{else}{$category.name}{/if}">
            </div>
        {/if}
    </div>
{/if}
