<div class="block-category visible--desktop">
    <div id="_desktop_category_header">
        <h1 class="display-2">{$category.name}{if isset($smarty.get.page) && $smarty.get.page > 1} <span class="small"> - Page {$smarty.get.page}</span>{/if}</h1>
    </div>
</div>
<div class="_mobile_category_header"></div>