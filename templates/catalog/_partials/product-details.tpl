<div class="card card__special mb-2" id="product-details" data-product="{$product.embedded_attributes|json_encode}">
    <div class="card-header" id="tab_heading_details">
       <h2 class="mb-0">{l s='Data sheet' d='Shop.Theme.Catalog'}</h2> 
    </div>
    <div class="card-body">

            {* {block name='product_reference'}

                {if isset($product_manufacturer->id)}
                <div class="product-manufacturer">

                    {if isset($manufacturer_image_url)}
                        <a href="{$product_brand_url}">
                          <img src="{$manufacturer_image_url}" class="img img-thumbnail manufacturer-logo" alt="{$product_manufacturer->name}">
                        </a>

                    {else}
                        <label class="label">
                        {l s='Brand' d='Shop.Theme.Catalog'}</label>
                        <span>
                          <a href="{$product_brand_url}">{$product_manufacturer->name}</a>
                        </span>

                    {/if}
                </div>

                {/if}

                {if isset($product.reference_to_display) && $product.reference_to_display neq ''}
                <div class="product-reference">
                  <label class="label">
                    {l s='Reference' d='Shop.Theme.Catalog'} </label>
                  <span>{$product.reference_to_display}</span>
                </div>

                {/if}

            {/block} *}

            {* {block name='product_quantities'}

                {if $product.show_quantities}
                <div class="product-quantities">
                  <label class="label">
                    {l s='In stock' d='Shop.Theme.Catalog'}</label>
                  <span data-stock="{$product.quantity}" data-allow-oosp="{$product.allow_oosp}">{$product.quantity} {$product.quantity_label}</span>
                </div>

                {/if}

            {/block} *}

            {* {block name='product_availability_date'}

                {if $product.availability_date}
                <div class="product-availability-date">
                  <label>
                    {l s='Availability date:' d='Shop.Theme.Catalog'} </label>
                  <span>{$product.availability_date}</span>
                </div>

                {/if}

            {/block} *}

            {* {block name='product_out_of_stock'}
      <div class="product-out-of-stock">
        {hook h='actionProductOutOfStock' product=$product}
      </div>
    {/block} *}

            {block name='product_features'}
                {if $product.grouped_features}
                    <div class="product-features table-responsive">
                        <table class="table table-striped data-sheet">
                            <tbody>
                                {foreach from=$product.grouped_features item=feature}
                                  {if $feature.name != 'auf Lager' && $feature.name != 'Stoffe'}
                                    <tr>
                                        <th scope="row" class="name">{$feature.name}</th>
                                        <td class="value">{$feature.value|escape:'htmlall'|nl2br nofilter}</td>
                                    </tr>
                                  {/if}
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                {/if}
            {/block}

            {* if product have specific references, a table will be added to product details section *}
            {* {block name='product_specific_references'}

                {if !empty($product.specific_references)}
                <section class="product-features">
                  <p class="h2">
                    {l s='Specific References' d='Shop.Theme.Catalog'}</p>
                    <dl class="data-sheet">

                    {foreach from=$product.specific_references item=reference key=key}
                            <dt class="name">{$key}</dt>
                            <dd class="value">{$reference}</dd>

                    {/foreach}
                    </dl>
                </section>

                {/if}

            {/block} *}

            {* {block name='product_condition'}

                {if $product.condition}
                <div class="product-condition">
                  <label class="label">
                    {l s='Condition' d='Shop.Theme.Catalog'} </label>
                  <span>{$product.condition.label}</span>
                </div>

                {/if}

            {/block} *}
    </div>
</div>