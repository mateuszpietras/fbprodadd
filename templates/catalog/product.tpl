{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file=$layout}
{* {hook h='ProductTabs' id_product=$product->id} *}
{block name='head_seo' prepend}
  <link rel="canonical" href="{$product.canonical_url}">
{/block}

{block name='content'}

  <section id="main">
    <div class="row">
      <div class="col-lg-7">
        {block name='page_content_container'}
          <div class="page-content--product" id="content">
            {block name='page_content'}
              {block name='product_cover_thumbnails'}
                {include file='catalog/_partials/product-cover-thumbnails.tpl'}
              {/block}
            {/block}
          </div>
        {/block}
        </div>
        <div class="col-lg-5">
          {block name='page_header_container'}
            {block name='page_header'}
              <h1 class="display-2">{block name='page_title'}{$product.name}{/block}</h1>
            {/block}
          {/block}

          {block name='product_additional_info'}
            {include file='catalog/_partials/product-additional-info.tpl'}
          {/block}

          {block name='product_prices'}
            {include file='catalog/_partials/product-prices.tpl'}
          {/block}

          <div class="product-information">
            {* {block name='product_description_short'}
              <div id="product-description-short-{$product.id}">{$product.description_short nofilter}</div>
            {/block} *}

            {if $product.is_customizable && count($product.customizations.fields)}
              {block name='product_customization'}
                {include file="catalog/_partials/product-customization.tpl" customizations=$product.customizations}
              {/block}
            {/if}

            <div class="product-actions">
              {block name='product_buy'}
                <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">
                  <input type="hidden" name="token" value="{$static_token}">
                  <input type="hidden" name="id_product" value="{$product.id}" id="product_page_product_id">
                  <input type="hidden" name="id_customization" value="{$product.id_customization}" id="product_customization_id">

                  {block name='product_variants'}
                    {include file='catalog/_partials/product-variants.tpl'}
                  {/block}

                  {* {block name='hook_display_product_combination_supplier'}
                    {hook h='displayProductCombinationSupplier' product=$product}
                  {/block} *}

                  {block name='product_pack'}
                    {if $packItems}
                      <section class="product-pack mb-4">
                        <p class="h4">{l s='This pack contains' d='Shop.Theme.Catalog'}</p>
                        {foreach from=$packItems item="product_pack"}
                          {block name='product_miniature'}
                            {include file='catalog/_partials/miniatures/pack-product.tpl' product=$product_pack}
                          {/block}
                        {/foreach}
                    </section>
                    {/if}
                  {/block}

                  {block name='product_discounts'}
                    {include file='catalog/_partials/product-discounts.tpl'}
                  {/block}

                  {block name='product_add_to_cart'}
                    {include file='catalog/_partials/product-add-to-cart.tpl'}
                  {/block}

                  {* {block name='product_refresh'}
                      {if !isset($product.product_url)}
                      <input class="product-refresh ps-hidden-by-js" name="refresh" type="submit" value="{l s='Refresh' d='Shop.Theme.Actions'}">
                      {/if}
                  {/block} *}
                </form>
              {/block}

            </div>

            {block name='hook_display_reassurance'}
              {hook h='displayReassurance'}
            {/block}

          {*block name='product_tabs'}
              {include file='catalog/_partials/product-tabs.tpl'}
          {/block*}

          </div>
      </div>
    </div>



    {block name='product_accessories'}
      {if $accessories}
        <section class="product-accessories clearfix">
          <h3 class="display-2"> {l s='You might also like' d='Shop.Theme.Catalog'}</h3>
          {assign var="productscount" value=$accessories|count}
          <div class="products products-slick spacing-md-top {if $productscount > 1} products--slickmobile{/if}" data-slick='{strip}
          {ldelim}
            "slidesToShow": 1,
            "slidesToScroll": 1,
            "mobileFirst":true,
            "arrows":true,
            "rows":0,
            "responsive": [
              {ldelim}
                "breakpoint": 992,
                "settings":
                {if $productscount > 4}
                {ldelim}
                "arrows":true,
                "slidesToShow": 4,
                "slidesToScroll": 4,
                "arrows":true
                {rdelim}
                {else}
                "unslick"
                {/if}
                {rdelim},
                {ldelim}
                "breakpoint": 720,
                "settings":
                {if $productscount > 3}
                {ldelim}
                "arrows":true,
                "slidesToShow": 3,
                "slidesToScroll": 3
                {rdelim}
                {else}
                "unslick"
                {/if}
                {rdelim},
                {ldelim}
                "breakpoint": 540,
                "settings":
                {if $productscount > 2}
                {ldelim}
                "arrows":true,
                "slidesToShow": 2,
                "slidesToScroll": 2
                {rdelim}
                {else}
                "unslick"
                {/if}
              {rdelim}
            ]
          {rdelim}{/strip}'>
            {foreach from=$accessories item="product_accessory"}
              {block name='product_miniature'}
                {include file='catalog/_partials/miniatures/product.tpl' product=$product_accessory}
              {/block}
            {/foreach}
          </div>
        </section>
      {/if}
    {/block}

    <div class="row my-5">
      {block name='product_description'}
        <div class="col-12 col-sm-12 col-md-4">
          <div class="card card__special">
            <div class="card-header">
              <h2 class="mb-0">{l s='Product description' d='Shop.Theme.Catalog'}</h2>
            </div>
            <div class="card-body product-description">{$product.description nofilter}</div>
          </div>
        </div>
      {/block}
      {* {block name='product_details'}
        <div class="col-12 col-sm-12 col-md-4">
          {include file='catalog/_partials/product-details.tpl'}
        </div>
      {/block} *}
      {block name='product_tabs'}
        <div class="col-12 col-sm-12 col-md-8">
          <div id="accordion">
            {hook h='ProductTabs' id_product=$product->id}
            {* {block name='product_details'}
              {include file='catalog/_partials/product-details.tpl'}
            {/block} *}
          </div>
        </div>
      {/block}
  </div>




    {block name='product_footer'}
      {hook h='displayFooterProduct' product=$product category=$category}
    {/block}

    {* {block name='product_images_modal'}
      {include file='catalog/_partials/product-images-modal.tpl'}
    {/block} *}

    {block name='page_footer_container'}
      <footer class="page-footer">{block name='page_footer'}{/block}</footer>
    {/block}
  </section>

{/block}
