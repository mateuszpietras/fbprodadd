{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{block name='inspirations'}
    {if $cms_pages}
      <section class="row align-items-center">
        {foreach from=$cms_pages item=cms_page}
          <article class="cms__item col-12 col-sm-6 mb-4 p-4">
            <a class="d-block" href="{$cms_page.link}">
                <figure class="inspirations__image">
                    <img class="img-fluid lazy" src="/img/cms/inspirantion/{$cms_page.id_cms}.jpg" width="617" height="344">
                </figure>					
                <article class="inspirations__content text-left">
                    <header>
                        <h3 class="inspirations__title">{$cms_page.meta_title|escape:'html':'UTF-8'}</h3>
                    </header>
                    <p class="inspirations__text">{$cms_page.meta_description|truncate:231:"..."}</p>
                    <p class="float-right"><span class="btn btn-primary">Jetzt entdecken</span></p>
                </article>                
            </a>
          </article>
        {/foreach}
      </section>
    {/if}
{/block}