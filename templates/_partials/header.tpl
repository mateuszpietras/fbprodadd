{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{block name='header_banner'}
    <div class="header__banner">
        {hook h='displayBanner'}
    </div>
{/block}

{block name='header_nav'}
    <div class="header__nav">
        {hook h='displayNav1'}
        <div class="header__container container">
            <div class="u-a-i-c d--flex-between visible--desktop">
                <div class="header-nav__right">
                    {hook h='displayNav2'}
                </div>
            </div>
        </div>
    </div>
{/block}

{block name='header_top'}
    <div class="header__wrapper">
        <div class="container header__top">
            <button class="visible--mobile header__link header__link--top" id="menu-icon" data-toggle="modal"
                data-target="#mobile_top_menu_wrapper" aria-label="toggle-menu">
                {* <i class="material-icons d-inline">&#xE5D2;</i> *}
                <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect x="3" y="6" width="20" height="1.4" fill="black" />
                    <rect x="3" y="12" width="20" height="1.4" fill="black" />
                    <rect x="3" y="18" width="20" height="1.4" fill="black" />
                </svg>
            </button>
            <a href="{$urls.base_url}"  aria-label="{$shop.name}">
                {* <img class="logo img-fluid" src="{$shop.logo}" alt="{$shop.name}"> *}
                <svg class="header__logo img-fluid" xmlns="http://www.w3.org/2000/svg" width="270" height="63" fill="none"
                    viewbox="0 0 273 63">
                    <path
                        d="M.157 18.359v27.108h9.714v-21.17l6.376 21.168h8.195l6.451-21.168v21.168h9.734V18.359H25.269l-4.95 16.513-4.583-16.513H.157zm46.406 0v27.108h10.501V18.359H46.564zm16.422 0v27.108h10.651v-8.267h6.268c1.307 0 2.236.204 2.786.606.562.405.897 1.099 1.008 2.089.06.525.106 1.271.13 2.236.035 1.65.231 2.762.585 3.338h11.677v-.422c-.782-.392-1.261-2.357-1.432-5.902l-.037-.863c-.086-1.871-.372-3.17-.863-3.904-.476-.744-1.313-1.369-2.511-1.871 1.395-.538 2.489-1.41 3.284-2.618.795-1.224 1.189-2.634 1.189-4.234 0-1.32-.269-2.481-.807-3.482-.525-1.016-1.295-1.819-2.308-2.419-.795-.476-1.903-.824-3.32-1.046-1.417-.231-3.304-.349-5.661-.349l-20.638.001zm51.999 0V33.39c0 2.285-.244 3.798-.733 4.545s-1.365 1.12-2.62 1.12c-1.101 0-1.913-.394-2.438-1.176s-.79-1.991-.79-3.627v-.398h-10.54V35.1c0 3.949 1.112 6.79 3.334 8.523 2.225 1.724 5.904 2.585 11.036 2.585 2.773 0 5.111-.329 7.018-.99 1.92-.658 3.373-1.644 4.364-2.952.684-.903 1.166-1.997 1.448-3.28s.423-3.507.423-6.669V18.359h-10.502zm23.369 0l-11.75 27.108h11.253l.972-2.66h11.53l.897 2.66h11.381l-10.96-27.108h-13.323zm26.133 0v27.108h9.9V29.595l11.383 15.871h11.778V18.359h-10.172v15.612l-10.461-15.614-12.428.002zm-90.854 6.875h5.865c1.845 0 3.073.177 3.683.531.624.343.936.954.936 1.833 0 .892-.329 1.528-.99 1.905-.648.381-1.773.568-3.373.568h-6.118l-.004-4.837zm71.134 1.355l3.262 9.53h-6.725l3.463-9.53zM239.228.949c-16.774 0-30.358 13.55-30.358 30.284s13.583 30.282 30.358 30.282c16.756 0 30.358-13.556 30.358-30.282S255.984.949 239.228.949zm-12.635 16.778c3.228 0 5.998.919 7.694 2.27 1.678 1.338 2.242 3.482 2.242 5.942 0 1.614-.419 3.148-1.239 4.603-.802 1.455-2.043 2.949-3.702 4.496-1.039.96-1.641 1.701-2.698 2.515-1.056.8-1.313 1.11-1.768 1.432l10.575.062v5.692h-20.166l.018-4.693c1.571-1.136 3.11-2.316 4.613-3.539 1.568-1.247 2.808-2.321 3.738-3.225 1.385-1.34 2.372-2.507 2.972-3.502.584-.996.875-1.98.875-2.952 0-1.166-.383-2.065-1.149-2.695-.747-.64-1.823-.962-3.228-.962-1.056 0-2.132.402-3.337.78-1.368.608-.658.303-1.878.78l-1.824-4.942c.802-.356 1.86-1.036 3.448-1.389 1.594-.356 3.228-.677 4.812-.677l.002.005zm30.977.027v15.264l3.3.026.037 5.393h-3.338v6.289h-6.272l.018-6.265-11.614-.026v-5.089l9.901-15.522 7.968-.069zm-6.254 6.252l-5.031 9.029 5.031.024v-9.052z"
                        fill="#e50000" />
                </svg>
            </a>
            <div class="header__search visible--desktop">
                {hook h='displaySearch'}
            </div>
            {* <button class="header__link header__link--top" id="menu-icon">
                <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect x="3" y="6" width="20" height="1.4" fill="black" />
                    <rect x="3" y="12" width="20" height="1.4" fill="black" />
                    <rect x="3" y="18" width="20" height="1.4" fill="black" />
                </svg>
            </button> *}
            <div class="header__icons header-top__col ">
                <button class='header__link visible--mobile p-0 header__button' data-target="#offcanvas_search_input " data-toggle="modal" aria-label="toggle-search">
                    <svg width="26" height="26" viewBox="0 0 26 26" fill="black" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M24.375 23.2294L18.2406 17.0625C19.7071 15.293 20.4357 13.0267 20.2749 10.7341C20.1142 8.44156 19.0765 6.29904 17.3773 4.75157C15.6782 3.2041 13.4483 2.37062 11.1508 2.42424C8.85321 2.47787 6.66461 3.41448 5.03955 5.03954C3.41449 6.6646 2.47788 8.8532 2.42425 11.1508C2.37062 13.4483 3.20411 15.6782 4.75158 17.3773C6.29905 19.0765 8.44157 20.1142 10.7341 20.2749C13.0267 20.4357 15.2931 19.7071 17.0625 18.2406L23.2294 24.375L24.375 23.2294ZM4.06251 11.375C4.06251 9.92872 4.49138 8.51493 5.29489 7.31239C6.09839 6.10986 7.24045 5.1726 8.57664 4.61913C9.91282 4.06567 11.3831 3.92085 12.8016 4.20301C14.2201 4.48516 15.5231 5.18161 16.5457 6.20428C17.5684 7.22695 18.2648 8.52992 18.547 9.9484C18.8292 11.3669 18.6843 12.8372 18.1309 14.1734C17.5774 15.5096 16.6401 16.6516 15.4376 17.4551C14.2351 18.2586 12.8213 18.6875 11.375 18.6875C9.43561 18.6875 7.57565 17.9171 6.20429 16.5457C4.83293 15.1744 4.06251 13.3144 4.06251 11.375Z"
                            fill="black" />
                    </svg> 
                    <span class="d-none">{l s='Search' d='Shop.Theme.Catalog'}</span>
                </button>      
                {hook h='displayTop'}
            </div>
        </div>
    </div>
    <div class="header__navbar">
        <div class="container">
            {hook h='displayNavFullWidth'}
        </div>
    </div>
{/block}