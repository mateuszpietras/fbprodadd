{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
 {extends file='customer/page.tpl'}

{block name='pageWrapperClass'}{/block}
{block name='pageHeaderClass'}{/block}
{block name='pageContentClass'}{/block}
{block name='pageFooterClass'}{/block}

{block name='page_title'}
  {l s='Your account' d='Shop.Theme.Customeraccount'}
{/block}

{block name='page_content'}
  <div class="row">
    <div class="col-12 mb-4">
        <div class="card p-3 p-md-4 p-xl-5">
          <div class="flex-row row align-items-center">
              <div class="col-12 col-md-4 col-xl-3 mb-4 mb-md-0">
                <h2><b>{l s='User profile' d='Shop.Theme.Customeraccount'}</b></h2>
                <h5>{l s='Manage your account and have convenient access to your data. Here you can freely update, delete or change the delivery address.' d='Shop.Theme.Customeraccount'}</h5>
              </div>
              <div class="col-12 col-md-4 col-xl-4 mb-5 mb-md-0">
                <h4><b>{l s='Go to edit your account information' d='Shop.Theme.Customeraccount'}</b></h4>
                <a class="btn btn-primary" id="identity-link" href="{$urls.pages.identity}">
                  {l s='Information' d='Shop.Theme.Customeraccount'}
                </a>  
              </div>
              <div class="col-12 col-md-4 col-xl-5 mb-3 mb-md-0">
                <h4><b>{l s='Additional information' d='Shop.Theme.Customeraccount'}</b></h4>
                <ul class="flex flex-column">
                  <li class="link-item">
                    <a id="identity-link" href="{$urls.pages.identity}">
                      {* <i class="material-icons">&#xE853;</i> *}
                      {l s='Information' d='Shop.Theme.Customeraccount'}
                    </a>      
                  </li>

                  {if $customer.addresses|count}
                    <li class="link-item">
                      <a id="addresses-link" href="{$urls.pages.addresses}">
                        {* <i class="material-icons">&#xE56A;</i> *}
                        {l s='Addresses' d='Shop.Theme.Customeraccount'}
                      </a>
                    </li>
                  {else}
                    <li class="link-item">
                      <a id="address-link" href="{$urls.pages.address}">
                        {* <i class="material-icons">&#xE567;</i> *}
                        {l s='Add first address' d='Shop.Theme.Customeraccount'}
                      </a>
                    </li>
                  {/if}

                  {block name='display_customer_account'}
                    {hook h='displayCustomerAccount'}
                  {/block}

                </ul>
              </div>
            </div>
        </div>
    </div>

    <div class="col-12 mb-4">
        <div class="card p-3 p-md-4 p-xl-5">
          <div class="flex-row row align-items-center">
              <div class="col-12 col-md-4 col-xl-3 mb-4 mb-md-0">
        <h2><b>{l s='Your order history' d='Shop.Theme.Customeraccount'}</b></h2>
        <h5>{l s='Get the access to detailed information about your orders. Here you will find its history and current statuses.' d='Shop.Theme.Customeraccount'}</h5>
      </div>
      <div class="col-12 col-md-4 col-xl-4 mb-5 mb-md-0">
        <h4><b>{l s='Go to orders history' d='Shop.Theme.Customeraccount'}</b></h4>
        <a class="btn btn-primary" id="history-link" href="{$urls.pages.history}">
        {l s='Order history and details' d='Shop.Theme.Customeraccount'}
        </a>
      </div>
      <div class="col-12 col-md-4 col-xl-5 mb-3 mb-md-0">
        <h4><b>{l s='Additional information' d='Shop.Theme.Customeraccount'}</b></h4>
        <ul class="flex flex-column">
          {if !$configuration.is_catalog}
            <li class="link-item">
              <a id="order-slips-link" href="{$urls.pages.order_slip}">
                {* <i class="material-icons">&#xE8B0;</i> *}
                {l s='Credit slips' d='Shop.Theme.Customeraccount'}
              </a>
            </li>
          {/if}

          {if !$configuration.is_catalog}
            <li class="link-item">
              <a id="history-link" href="{$urls.pages.history}">
                {* <i class="material-icons">&#xE916;</i> *}
                {l s='Order history and details' d='Shop.Theme.Customeraccount'}
              </a>
            </li>
          {/if}

          {if $configuration.voucher_enabled && !$configuration.is_catalog}
            <li class="link-item">
              <a id="discounts-link" href="{$urls.pages.discount}">
                {* <i class="material-icons">&#xE54E;</i> *}
                {l s='Vouchers' d='Shop.Theme.Customeraccount'}
              </a>
            </li>
          {/if}

          {if $configuration.return_enabled && !$configuration.is_catalog}
            <li class="link-item">
              <a id="returns-link" href="{$urls.pages.order_follow}">
                {* <i class="material-icons">&#xE860;</i> *}
                {l s='Merchandise returns' d='Shop.Theme.Customeraccount'}
              </a>
            </li>
          {/if}
        </ul>
        </div>
        </div>
    </div>
</div>
    {widget name='mwishlist'}
  </div>
{/block}


{block name='page_footer'}
  {block name='my_account_links'}
    <div class="text-sm-center">
      <a class="btn btn-outline-secondary" href="{$logout_url}" >
        {l s='Sign out' d='Shop.Theme.Actions'}
      </a>
    </div>
  {/block}
{/block}
