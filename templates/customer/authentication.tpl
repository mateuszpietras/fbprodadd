{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}
{* {block name='contentWrapperClass'}col-12 col-lg-8 offset-lg-2{/block} *}

{block name='page_content'}
    <div class="row justify-content-center">
      <div class="col-12 col-md-6 col-lg-5 col-xl-4 mb-5 page__box page__box--left">
        <h3 class="display-3">{l s='Create new account' d='Shop.Theme.Customeraccount'}</h3>
        <p>{l s='Create an account and get started to change your home for betters' d='Shop.Theme.Customeraccount'}</p>
        <a class="btn btn-primary" href="{$urls.pages.register}" data-link-action="display-register-form">
            {l s='Create account' d='Shop.Theme.Customeraccount'}
        </a>
      </div>
      <div class="col-12 col-md-6 col-lg-5 col-xl-4 mb-5 page__box">
        {block name='login_form_container'}
            <h3 class="display-3">{l s='Log in to your account' d='Shop.Theme.Customeraccount'}<h3>
            {render file='customer/_partials/login-form.tpl' ui=$login_form}
            {block name='display_after_login_form'}
              {hook h='displayCustomerLoginFormAfter'}
            {/block}
        {/block}
      </div>
    </div>
{/block}
{block name='page_footer'}
  <p class="mb-0 d-flex justify-content-center align-items-center">
    <svg class="mr-1" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M5.443 1.991C4.52848 2.23685 3.6199 2.50426 2.718 2.793C2.63638 2.81819 2.56353 2.8659 2.5078 2.93065C2.45208 2.99539 2.41576 3.07454 2.403 3.159C1.87 7.056 3.1 9.9 4.567 11.773C5.303 12.713 6.1 13.409 6.764 13.866C7.097 14.094 7.39 14.26 7.621 14.366C7.737 14.419 7.831 14.455 7.903 14.476C7.93475 14.4862 7.96716 14.4942 8 14.5C8.007 14.499 8.038 14.495 8.097 14.477C8.169 14.455 8.263 14.419 8.379 14.366C8.609 14.26 8.904 14.094 9.236 13.866C10.0686 13.2818 10.8092 12.5763 11.433 11.773C12.9 9.9 14.13 7.056 13.597 3.159C13.5842 3.07454 13.5479 2.99539 13.4922 2.93065C13.4365 2.8659 13.3636 2.81819 13.282 2.793C12.656 2.593 11.6 2.267 10.557 1.991C9.491 1.71 8.51 1.5 8 1.5C7.49 1.5 6.51 1.71 5.443 1.991ZM5.187 1.025C6.23 0.749 7.337 0.5 8 0.5C8.662 0.5 9.77 0.749 10.813 1.025C11.7433 1.27477 12.6676 1.54652 13.585 1.84C14.113 2.008 14.511 2.463 14.588 3.024C15.161 7.221 13.832 10.331 12.221 12.389C11.5345 13.2722 10.7194 14.0475 9.803 14.689C9.48435 14.9122 9.14749 15.1082 8.796 15.275C8.526 15.399 8.238 15.5 8 15.5C7.762 15.5 7.474 15.399 7.204 15.275C6.85243 15.1084 6.51557 14.9123 6.197 14.689C5.28094 14.0474 4.46622 13.2721 3.78 12.389C2.167 10.331 0.839 7.221 1.412 3.024C1.45031 2.75207 1.56485 2.49655 1.74235 2.28701C1.91986 2.07748 2.15307 1.92249 2.415 1.84C3.33245 1.54652 4.25671 1.27478 5.187 1.025Z" fill="black"/>
      <path d="M9.5 6.5C9.5 6.89782 9.34196 7.27936 9.06066 7.56066C8.77936 7.84196 8.39782 8 8 8C7.60218 8 7.22064 7.84196 6.93934 7.56066C6.65804 7.27936 6.5 6.89782 6.5 6.5C6.5 6.10218 6.65804 5.72064 6.93934 5.43934C7.22064 5.15804 7.60218 5 8 5C8.39782 5 8.77936 5.15804 9.06066 5.43934C9.34196 5.72064 9.5 6.10218 9.5 6.5Z" fill="black"/>
      <path d="M7.411 8.034C7.43062 7.91743 7.49091 7.81158 7.58116 7.73524C7.67141 7.65891 7.78579 7.61701 7.904 7.617H8.06C8.1776 7.61713 8.29139 7.6587 8.38138 7.73442C8.47136 7.81014 8.53177 7.91515 8.552 8.031L8.899 10.031C8.91139 10.1028 8.90793 10.1765 8.88886 10.2468C8.8698 10.3172 8.83558 10.3825 8.78862 10.4382C8.74165 10.4939 8.68307 10.5387 8.61698 10.5694C8.55089 10.6001 8.47888 10.616 8.406 10.616H7.571C7.49842 10.616 7.42672 10.6001 7.36088 10.5696C7.29503 10.5391 7.23662 10.4946 7.18971 10.4392C7.1428 10.3838 7.10851 10.3189 7.08922 10.2489C7.06993 10.1789 7.0661 10.1056 7.078 10.034L7.411 8.034Z" fill="black"/>
    </svg>
  {l s='You are safe! This store is secured with an SSL certificate' d='Shop.Theme.Customeraccount'}
  </p>
{/block}