{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}

{block name='page_title'}
  
{/block}

{block name='page_content'}
    <div class="row justify-content-center">
      <div class="col-12 col-md-6 col-lg-5 mb-5 page__box page__box--left">
        <h3 class="display-3">{l s='Create an account' d='Shop.Theme.Customeraccount'}</h3>
        <p>{l s='Create an account and get started to change your home for betters' d='Shop.Theme.Customeraccount'}</p>
        <p>{l s='Already have an account?' d='Shop.Theme.Customeraccount'} <a href="{$urls.pages.authentication}">{l s='Log in instead!' d='Shop.Theme.Customeraccount'}</a></p>
      </div>
      <div class="col-12 col-md-6 col-lg-5 mb-5 page__box">
      {block name='register_form_container'}
        {$hook_create_account_top nofilter}
        <div class="register-form">
          {render file='customer/_partials/customer-form.tpl' ui=$register_form}
        </div>
      {/block}
      </div>
    </div>
{/block}
{block name='page_footer_container'}
{/block}
