{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file=$layout}

{block name='content'}

  <section class="mt-4" id="main">

    <div class="cart-grid{*row*}">
      <!-- Left Block: cart product informations & shpping -->
      <div class="cart-grid-body{*col-12 col-lg-8*}">
        <h1 class="page__title">
          {l s='Your Shopping Cart' d='Shop.Theme.Checkout'}
          <small class="text-muted"><span class="js-cart-products-nb">{$cart.products_count}</span> {l s='items' d='Shop.Theme.Checkout'}</small>
        </h1>
        <!-- cart products detailed -->
        <div class="cart-container mb-3">
            
          <div class="cart__card-body js-cart__card-body">
            <div class="cart__card-loader"><div class="spinner-border" role="status"><span class="sr-only">{l s='Loading...' d='Shop.Theme.Global'}</span></div></div>
          {block name='cart_overview'}
            {include file='checkout/_partials/cart-detailed.tpl' cart=$cart}
          {/block}
          </div>
        </div>
        <div class="border-0">
        {block name='continue_shopping'}
          <a class="btn-link" href="{$urls.pages.index}">
            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M15 8.00001C15 7.8674 14.9473 7.74022 14.8536 7.64645C14.7598 7.55268 14.6326 7.50001 14.5 7.50001H2.70701L5.85401 4.35401C5.9005 4.30752 5.93737 4.25233 5.96253 4.19159C5.98769 4.13085 6.00064 4.06575 6.00064 4.00001C6.00064 3.93426 5.98769 3.86916 5.96253 3.80842C5.93737 3.74768 5.9005 3.69249 5.85401 3.64601C5.80752 3.59952 5.75233 3.56264 5.69159 3.53748C5.63085 3.51232 5.56575 3.49937 5.50001 3.49937C5.43426 3.49937 5.36916 3.51232 5.30842 3.53748C5.24769 3.56264 5.1925 3.59952 5.14601 3.64601L1.14601 7.64601C1.09945 7.69245 1.0625 7.74763 1.0373 7.80837C1.01209 7.86912 0.999115 7.93424 0.999115 8.00001C0.999115 8.06577 1.01209 8.13089 1.0373 8.19164C1.0625 8.25238 1.09945 8.30756 1.14601 8.35401L5.14601 12.354C5.1925 12.4005 5.24769 12.4374 5.30842 12.4625C5.36916 12.4877 5.43426 12.5006 5.50001 12.5006C5.56575 12.5006 5.63085 12.4877 5.69159 12.4625C5.75233 12.4374 5.80752 12.4005 5.85401 12.354C5.9005 12.3075 5.93737 12.2523 5.96253 12.1916C5.98769 12.1308 6.00064 12.0657 6.00064 12C6.00064 11.9343 5.98769 11.8692 5.96253 11.8084C5.93737 11.7477 5.9005 11.6925 5.85401 11.646L2.70701 8.50001H14.5C14.6326 8.50001 14.7598 8.44733 14.8536 8.35356C14.9473 8.25979 15 8.13261 15 8.00001Z" fill="black"/>
            </svg>
            {l s='Continue shopping' d='Shop.Theme.Actions'}
          </a>
        {/block}
        </div>

        <!-- shipping informations -->
        {block name='hook_shopping_cart_footer'}
          {hook h='displayShoppingCartFooter'}
        {/block}
      </div>

      <!-- Right Block: cart subtotal & cart total -->
      <div class="cart-grid-right{*col-12 col-lg-4 mt-3 mt-lg-0*}">
        {block name='cart_summary'}
          <div class="cart-summary mb-5">
            <p class="page__title">{l s='Your summary' d='Shop.Theme.Checkout'}</p>
            <div class="cart-summary__summary">
            {block name='hook_shopping_cart'}
              {hook h='displayShoppingCart'}
            {/block}

            {block name='cart_totals'}
              {include file='checkout/_partials/cart-detailed-totals.tpl' cart=$cart}
            {/block}
            </div>
            <div class="cart-summary__footer">
            {block name='cart_actions'}
              {include file='checkout/_partials/cart-detailed-actions.tpl' cart=$cart}
            {/block}
            </div>
            {block name='cart_summary_voucher'}
                {include file='checkout/_partials/cart-voucher.tpl'}
            {/block}
          </div>
        {/block}

        {block name='hook_reassurance'}
          {hook h='displayReassurance'}
        {/block}

      </div>

    </div>
  </section>
{/block}
