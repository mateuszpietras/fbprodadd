<section class="homeproducts-carousel">
    <h3 class="display-2 text-center mb-5">{l s='Our stars' mod='blockhomeproductscarousel'}</h3>
    <div class="homeproducts-carousel__wrapper slick-slider" data-slick={strip}'{literal}{
    "autoplay": true,
    "slidesToShow": 1,
    "autoplaySpeed":{/literal}5000{literal}
    }{/literal}'{/strip}>
        {foreach from=$products item=product}
            {block name='homeproducts_product'}
                {include file='module:blockhomeproductscarousel/views/templates/hook/_partials/product.tpl' product=$product}
            {/block}
        {/foreach}
    </div>
</section>