<article class="homeproducts-product">
    <div class="homeproducts-product__wrapper row align-items-center">
        <div class="col-12 col-sm-6 col-md-6 col-xl-5 homeproducts-product__image mb-3">
            <a class="rc ratio16_9 d-block" href="{$product.link}">
                <img class="img-fluid lazyload"
                    data-src="/modules/blockhomeproductscarousel/images/{$product.id_product}.jpg" width="706" height="397" />
            </a>
        </div>
        <div class="col-12 col-sm-6 col-md-6 col-xl-7 homeproducts-product__content p-5">
            <h3 class="homeproducts-product__name display-3">
                <a class="homeproducts-product__link" href="{$product.link}">
                    {$product.name}
                </a>
            </h3>
            {block name='product_reviews'}
                {hook h='displayProductListReviews' product=$product}
            {/block}
            {block name='product_price_and_shipping'}
                {if $product.show_price}
                    <div class="product-price-and-shipping mb-2">
                        {if $product.has_discount}
                            {hook h='displayProductPriceBlock' product=$product type="old_price"}
                            <span class="sr-only">{l s='Regular price' d='Shop.Theme.Catalog'}</span>
                            <span class="regular-price">{$product.regular_price}</span>
                        {/if}
                        {hook h='displayProductPriceBlock' product=$product type="before_price"}
                        <span class="sr-only">{l s='Price' d='Shop.Theme.Catalog'}</span>
                        <span class="price{if $product.has_discount} current-price-discount{/if} display-2">{$product.price}</span>
                        {hook h='displayProductPriceBlock' product=$product type='unit_price'}
                        {hook h='displayProductPriceBlock' product=$product type='weight'}
                    </div>
                {/if}
            {/block}

            <div class="homeproducts-product__description mb-3">
                {$product.description nofilter}
            </div>


            {block name='delivery_time'}
                <div class="d-flex align-items-center mb-2">
                    <i class="material-icons mr-1">local_shipping</i>
                    {hook h='displayProductCombinationSupplier' product=$product type="list"}
                </div>
            {/block}
            {block name='block_features'}
                <div class="d-flex align-items-center mb-2">
                    <i class="material-icons mr-1">thumb_up_alt</i>
                    {l s='Free shipping' d='Modules.Blockhomeproductscarousel.Product'}
                </div>
                <div class="d-flex align-items-center">
                    <i class="material-icons mr-1">palette</i>
                    {l s='More colors to choose' d='Modules.Blockhomeproductscarousel.Product'}
                </div>
            {/block}

            <p class="text-right">
                <a class="btn btn-secondary" href="{$product.link}">
                    {l s='See more' d='Modules.Blockhomeproductscarousel.Product'}
                </a>
            </p>
        </div>
    </div>
</article>