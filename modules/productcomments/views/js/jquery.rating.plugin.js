/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */
const customRating = function(wrapper, resting){
  this.wrapper = wrapper;
  this.stars = false;
  this.grade = null; 
  this.input = false;

  this.init = function(resting) {
    if(this.wrapper){
      this.wrapper = wrapper
      this.stars = wrapper.querySelectorAll('.grade__star');
      this.grade = parseInt(wrapper.getAttribute('data-grade'));
      this.refreshGrade()
      if(resting && resting === true){
        wrapper.classList.add('grade__wrapper--static')
      } else {
        const criterionName = this.wrapper.getAttribute('data-input');
        this.generateInput(criterionName);
        this.changeGradeOnClick()
        this.hoverGrade()
        this.resetGrade() 
      }
    } else {
      console.log('Brak ocen')
    }      
  };

  this.changeGradeOnClick = function(){
    const _this = this;

    this.stars.forEach(star => {
      star.addEventListener('click', () => {
        // zabezpieczyc przed klikaniem prawym przyciskiem myszy
        const starGrade = star.getAttribute('data-grade')
        if(starGrade !== _this.grade){
          _this.input.value = starGrade;
          _this.grade = starGrade;
          _this.wrapper.setAttribute('data-grade', starGrade);
          _this.refreshGrade()
        } 
      })
    })
  };

  this.generateInput = function(name){
    const input = document.createElement('input');
    input.type = 'number';
    input.id = name;
    input.name = name;
    input.style.display = 'none'
    input.value = this.grade;
    this.input = input
    this.wrapper.parentNode.appendChild(input)
  }

  this.hoverGrade = function(){
    const _this = this

    this.stars.forEach(star => {
      star.addEventListener('mouseover', () => {
        const starGrade = star.getAttribute('data-grade')
        if(starGrade !== _this.grade){
          _this.refreshGrade(starGrade)
        } 
      })
    })     
  };

  this.resetGrade = function(){
    const _this = this;

    _this.wrapper.addEventListener('mouseleave', () => {
      _this.refreshGrade()
    })
  },

  this.refreshGrade = function(rate){
    const grade = rate ? rate : this.grade
    
    this.stars.forEach(star => star.classList.remove('grade__star--filled'))
    for(let i = 0; i < grade; i++){
      this.stars[i].classList.add('grade__star--filled')
    }      
  };

  this.init(resting);

}