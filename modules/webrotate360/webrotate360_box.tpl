{*
 * @author WebRotate 360 LLC
 * @copyright Copyright (C) 2019 WebRotate 360 LLC. All rights reserved.
 * @license GNU General Public License version 2 or later (http://www.gnu.org/copyleft/gpl.html).
 * @version 2.6.0
 * @module WebRotate 360 Product Viewer for Prestashop.
*}
<div class="webrotate__box {$placeholder|replace:'.':''|replace:'#':''}">

</div>