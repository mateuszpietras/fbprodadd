{*
 * @author WebRotate 360 LLC
 * @copyright Copyright (C) 2019 WebRotate 360 LLC. All rights reserved.
 * @license GNU General Public License version 2 or later (http://www.gnu.org/copyleft/gpl.html).
 * @version 2.6.0
 * @module WebRotate 360 Product Viewer for Prestashop.
*}

<div class="product-tab-content">
    <div class="alert alert-info">
        <p class="alert-text">Visit <a href="https://www.webrotate360.com/products/cms-and-e-commerce-plugins/plugin-for-prestashop.aspx?section=Installation" class="alert-link" target="_blank">this page</a> for additional information on installing and configuring this module.<br/>
            Use this demo config file URL for testing: <a href="{$demo_xml|escape:'html':'UTF-8'}" target="_blank">{$demo_xml|escape:'html':'UTF-8'}</a></p>
    </div>
    <fieldset class="form-group">
        <label class="col-sm-2 control-label" for="uploadable_files">
            Config File URL (xml)
            <span class="help-box" data-toggle="popover" data-placement="right" data-content="This file is created on publish in WebRotate 360 SpotEditor (desktop software) under published/360_assets/your-folder. Upload the entire folder (your-folder) to your Prestashop FTP first and enter the xml file URL here (relative URL works best)."></span>
        </label>
        <input name="config_file_url" class="form-control" value="{$config_file_url|escape:'html':'UTF-8'}" type="text">
    </fieldset>
    <fieldset class="form-group">
        <label class="col-sm-2 control-label" for="uploadable_files">
            Root Path
            <span class="help-box" data-toggle="popover" data-placement="right" data-content="If Master Config is set in the module settings, you can enter Root Path pointing to a folder with images on this server or some external server and leave Config File URL empty. Refer to the Installation page linked at the top for more details (under 'Load images from CDN or use master config')."></span>
        </label>
        <input name="root_path" class="form-control" value="{$root_path|escape:'html':'UTF-8'}" type="text">
    </fieldset>
</div>


