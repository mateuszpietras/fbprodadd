/**
 * @author WebRotate 360 LLC
 * @copyright Copyright (C) 2019 WebRotate 360 LLC. All rights reserved.
 * @license GNU General Public License version 2 or later (http://www.gnu.org/copyleft/gpl.html).
 * @version 2.6.0
 * @module WebRotate 360 Product Viewer for Prestashop.
 */

function wr360QueryGetParameterByName(name){
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

jQuery(document).ready(function(){
    var popup360Elm = jQuery("#wr360PlayerId20");
    if (popup360Elm.length == 1) {
        // var backgroundColor = wr360QueryGetParameterByName("background");
        // if (backgroundColor && backgroundColor.length > 0)
        //    popup360Elm.css("background-color", backgroundColor);

        popup360Elm.rotator({
            licenseFileURL: wr360QueryGetParameterByName("lic"),
            graphicsPath: wr360QueryGetParameterByName("grphpath"),
            configFileURL: wr360QueryGetParameterByName("config"),
            rootPath: wr360QueryGetParameterByName("root"),
            googleEventTracking: wr360QueryGetParameterByName("analyt") === "true",
            viewName: wr360QueryGetParameterByName("viewname")
        });
    }
 });


