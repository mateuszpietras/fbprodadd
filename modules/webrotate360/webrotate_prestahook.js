/**
 * @author WebRotate 360 LLC
 * @copyright Copyright (C) 2019 WebRotate 360 LLC. All rights reserved.
 * @license GNU General Public License version 2 or later (http://www.gnu.org/copyleft/gpl.html).
 * @version 2.6.0
 * @module WebRotate 360 Product Viewer for Prestashop.
*/

var rotator;

prestashop.on('updatedProduct', function (event) {
    WR360Init();
});

$(document).ready(function() {
    WR360Init();
});

function WR360Init() {
    if (typeof "__webrotate360" === "undefined")
        return;

    if (__webrotate360.is_viewer_popup) {
        WR360InitPopupViewer(__webrotate360);
        return;
    }
    // console.log(__webrotate360)
    WR360InitEmbeddedViewer(__webrotate360);
}

function WR360InitEmbeddedViewer(cfg) {
    var imageDiv = $(cfg.placeholder);
    if (imageDiv.length != 1)
        return;

    var oldReplaceWith = $.fn.replaceWith;

    $.fn.replaceWith = function() {
        var refreshElementClass = "images-container";
        if ($(this).hasClass(refreshElementClass) &&
            cfg.placeholder.indexOf(refreshElementClass) >= 0)
            return;
        oldReplaceWith.apply(this, arguments);
    };

    if (cfg.viewer_width.length > 0)
        imageDiv.css({ width: cfg.viewer_width });

    cfg.min_height = $('.product-cover__image').innerHeight()

    if (cfg.viewer_height.length > 0)
        imageDiv.css({ height: cfg.viewer_height });

    imageDiv.html("<div id='wr360PlayerId'></div>");
    imageDiv.css({
        visibility: "visible",
        padding: 0
    });

    rotator = WR360.ImageRotator.Create("wr360PlayerId");

    rotator.settings.graphicsPath        = cfg.graphics_path;
    rotator.settings.configFileURL       = cfg.config_file_url;
    rotator.settings.rootPath            = cfg.root_path;
    rotator.settings.responsiveBaseWidth = parseInt(cfg.base_width);
    rotator.settings.responsiveMinHeight = parseInt(cfg.min_height);
    rotator.settings.googleEventTracking = cfg.use_analytics === "true";
    rotator.licenseFileURL               = cfg.license_path;

    if (cfg.api_callback.length > 0) {
        var fn = window[cfg.api_callback];
        if (typeof fn === "function")
            rotator.settings.apiReadyCallback = fn;
    }

    $(document).on('click', '.js-webrotate', function(){
        $('.js-thumb-selected').removeClass('js-thumb-selected')
        $('.thumb-container__wrapper').removeClass('thumb-container__wrapper--selected')

        $(this).addClass('thumb-container__wrapper--selected')
        $('.webrotate__box').addClass('webrotate__box--active')
        rotator.runImageRotator();
    })

    // $(".images-container .thumb-container").click(function() {
    //     $("#product-modal").modal("show");
    // });
}

function WR360InitPopupViewer(cfg) {
    var popupElm = $(cfg.placeholder);
    if (popupElm.length <= 0)
        return;

    popupElm.html("<a href='"
        + cfg.popup_href
        + "'"
        + "data-rel='prettyPhoto'><img src='"
        + cfg.popup_thumb
        + "'/></a>");

    $("a[data-rel^='prettyPhoto']").prettyPhoto({
        animation_speed: 0,
        deeplinking: false,
        theme: cfg.popup_skin
    });

    popupElm.css({ visibility: "visible" });
}