{*
 * @author WebRotate 360 LLC
 * @copyright Copyright (C) 2019 WebRotate 360 LLC. All rights reserved.
 * @license GNU General Public License version 2 or later (http://www.gnu.org/copyleft/gpl.html).
 * @version 2.6.0
 * @module WebRotate 360 Product Viewer for Prestashop.
*}
<div class="thumb-container js-webrotate-container">
    <div class="thumb-container__wrapper js-webrotate">
        <img 
            class="thumb"
            src="/modules/webrotate360/360.png" 
            width="94" 
            height="94"
            alt="Webrotate"
        />
    </div>
</div>
