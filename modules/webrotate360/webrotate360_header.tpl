{*
 * @author WebRotate 360 LLC
 * @copyright Copyright (C) 2019 WebRotate 360 LLC. All rights reserved.
 * @license GNU General Public License version 2 or later (http://www.gnu.org/copyleft/gpl.html).
 * @version 2.6.0
 * @module WebRotate 360 Product Viewer for Prestashop.
*}

{if isset($webrotate360)}
    <style type="text/css">
        {$webrotate360.placeholder|escape:'html':'UTF-8'}{literal}{visibility: hidden;}{/literal}
    </style>
    <script type="text/javascript">
        var __webrotate360 = {$webrotate360json nofilter};
    </script>
{/if}






