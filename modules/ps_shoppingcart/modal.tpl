{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div id="blockcart-modal" class="modal fade blockcart-modal fixed-right" tabindex="-1" role="dialog"
    aria-labelledby="blockcart-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-dialog__offcanvas modal-dialog__offcanvas--md modal-dialog__offcanvas--right"
        role="document">
        <div class="modal-content">
            <div class="modal-header modal-header--success">
                <div class="modal-title" id="blockcart-modal-label">
                    {l s='%s has been successfully added to your shopping cart' sprintf=[$product.name] d='Shop.Theme.Checkout'}
                </div>
                <p class="mt-3 text-right mb-0">
                    <a href="{$cart_url}" class="modal-header__link modal-header__link--success">
                        {l s='Proceed to checkout' d='Shop.Theme.Actions'}
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11 3L15 8M15 8L11 13M15 8H1" stroke="#335E22" stroke-width="2"
                                stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                    </a>

                </p>
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="{l s='Close' d='Shop.Theme.Global'}">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="media">
                            <img class="product-image modal-cart__image" src="{$product.cover.bySize.pdt_180.url}"
                                alt="{$product.cover.legend}" width="{$product.cover.bySize.pdt_180.width}"
                                height="{$product.cover.bySize.pdt_180.height}">
                            <div class="media-body">
                                <p class="h5 product-name modal-cart__name">{$product.name}</p>
                                <p class="product-price modal-cart__price">{$product.price}</p>
                                {hook h='displayProductPriceBlock' product=$product type="unit_price"}
                                {foreach from=$product.attributes item="property_value" key="property"}
                                    <small class="d-block">{l s='%label%:' sprintf=['%label%' => $property] d='Shop.Theme.Global'}
                                           {$property_value}
                                    </small>
                                {/foreach}
                                <small classs="d-block">
                                    {l s='Quantity:' d='Shop.Theme.Checkout'}&nbsp;{$product.cart_quantity}
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="cart-content">
                    {if $cart.products_count > 1}
                        <p class="cart-products-count">
                            {l s='There are %products_count% items in your cart.' sprintf=['%products_count%' => $cart.products_count] d='Shop.Theme.Checkout'}
                        </p>
                    {else}
                        <p class="cart-products-count">
                            {l s='There is %product_count% item in your cart.' sprintf=['%product_count%' =>$cart.products_count] d='Shop.Theme.Checkout'}
                        </p>
                    {/if}
                    <p class="d--flex-between"><span>{l s='Subtotal:' d='Shop.Theme.Checkout'}</span>&nbsp;<span
                            class="value">{$cart.subtotals.products.value}</span></p>
                    <p class="d--flex-between"><span>{l s='Shipping:' d='Shop.Theme.Checkout'}</span>&nbsp;<span
                            class="value">{$cart.subtotals.shipping.value}
                            {hook h='displayCheckoutSubtotalDetails' subtotal=$cart.subtotals.shipping}</span></p>

                    {if !$configuration.display_prices_tax_incl && $configuration.taxes_enabled}
                        <p class="d--flex-between">
                            <span>{$cart.totals.total.label}&nbsp;{$cart.labels.tax_short}</span>&nbsp;<span>{$cart.totals.total.value}</span>
                        </p>
                        <p class="product-total d--flex-between">
                            <span>{$cart.totals.total_including_tax.label}</span>&nbsp;<span
                                class="value">{$cart.totals.total_including_tax.value}</span></p>
                    {else}
                        <p class="product-total d--flex-between">
                            <span>{$cart.totals.total.label}&nbsp;{if $configuration.taxes_enabled}{$cart.labels.tax_short}{/if}</span>&nbsp;<span
                                class="value">{$cart.totals.total.value}</span></p>
                    {/if}

                    {if $cart.subtotals.tax}
                        <p class="product-tax small">
                            {l s='%label%:' sprintf=['%label%' => $cart.subtotals.tax.label] d='Shop.Theme.Global'}&nbsp;<span
                                class="value">{$cart.subtotals.tax.value}</span></p>
                    {/if}
                </div>
                <div class="cart-buttons">
                    <button type="button" class="btn btn-block btn-link"
                        data-dismiss="modal">{l s='Continue shopping' d='Shop.Theme.Actions'}</button>
                    <a href="{$cart_url}"
                        class="btn btn-block btn-primary">{l s='Proceed to checkout' d='Shop.Theme.Actions'}</a>
                </div>
            </div>
        </div>
    </div>
</div>