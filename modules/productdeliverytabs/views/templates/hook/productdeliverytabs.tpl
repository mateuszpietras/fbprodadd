{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $combination_supplier.supplier_name}
    {if $type == 'product'}
        <div class="form-group productdeliverytabs">
            <label class="label productdeliverytabs__label">{l s='Delivery time:' mod='productdeliverytabs'} </label>
            <p class="small productdeliverytabs__time{if $combination_supplier.label|escape:'htmlall':'UTF-8'} productdeliverytabs__time--labeled{/if}"
                {if $combination_supplier.color|escape:'htmlall':'UTF-8'}
                style="color: {$combination_supplier.color|escape:'htmlall':'UTF-8'};" {/if}
                id="deliverytime_{$combination_supplier.id_product|escape:'htmlall':'UTF-8'}">
                {$combination_supplier.supplier_name|escape:'htmlall':'UTF-8'}
            </p>
        </div>
    {else}
        <div class="productdeliverytabs">
            <span class="productdeliverytabs__label">{l s='Delivery time:' mod='productdeliverytabs'} </span>
            <span
                class="productdeliverytabs__time{if $combination_supplier.label|escape:'htmlall':'UTF-8'} productdeliverytabs__time--labeled{/if}"
                {if $combination_supplier.color|escape:'htmlall':'UTF-8'}
                style="color: {$combination_supplier.color|escape:'htmlall':'UTF-8'};" {/if}>
                {$combination_supplier.supplier_name|escape:'htmlall':'UTF-8'}
            </span>
        </div>
    {/if}
{/if}