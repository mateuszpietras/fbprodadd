{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

{function name="categories" nodes=[] depth=0}
  {strip}
    {if $nodes|count}
        {foreach from=$nodes item=node}

          <article class="box" data-depth="{$depth}">
            <a class="box__wrapper" href="{$node.link}">
              <div class="box__thumbnail rc ratio1_1">
                <img class="img-fluid lazyload" alt="{$node.name}" data-src="{$link->getCatImageLink($node.name, $node.id, 'category_default')}" width="200" height="200" />
              </div>
              <h3 class="box__title">
                {$node.name}
              </h3>
            </a>
          </article>

          {* <li data-depth="{$depth}" class="category-sub__item category-sub__item--{$depth}{if $depth===0} clearfix{/if}">
            {if $depth===0}
              <a href="{$node.link}" {if $node.children}class="float-left"{/if}>{$node.name}</a>
              {if $node.children}
                <div class="float-right navbar-toggler collapse-icons" data-toggle="collapse" data-target="#exCollapsingNavbar{$node.id}">
                  <i class="material-icons add">&#xE145;</i>
                  <i class="material-icons remove">&#xE15B;</i>
                </div>
                <div class="collapse float-left clearfix w-100" id="exCollapsingNavbar{$node.id}">
                  {categories nodes=$node.children depth=$depth+1}
                </div>
              {/if}
            {else}
              <a class="category-sub-link" href="{$node.link}">{$node.name}</a>
              {if $node.children}
                <i class="material-icons icon-collapse cursor-pointer" data-toggle="collapse" data-target="#exCollapsingNavbar{$node.id}">&#xE313;</i>
                <div class="collapse float-left clearfix w-100" id="exCollapsingNavbar{$node.id}">
                  {categories nodes=$node.children depth=$depth+1}
                </div>
              {/if}
            {/if}
          </li> *}
        {/foreach}
      {* </ul> *}
    {/if}
  {/strip}
{/function}

{if $page.page_name == 'category'}
  {if $categories.children}
    <div class="d-lg-none d-xl-none">
      <button type="button" id="subcategories_toggle" data-target="#offcanvas_subcategories" data-toggle="modal" class="btn btn-secondary">{l s='Subcategories' d='Shop.Theme.Global'}</button> 
    </div>
  {/if}
<div id="_desktop_subcategories" class="block-categories visible--desktop">
  <div class="categories" data-slick='{strip}
    {ldelim}
    "slidesToShow": 1,
    "slidesToScroll": 1,
    "mobileFirst":true,
    "arrows":true,
    "rows":0,
    "responsive": [
      {ldelim}
        "breakpoint": 992,
        "settings":
        {if count($categories.children) > 4}
        {ldelim}
        "arrows":true,
        "slidesToShow": 6,
        "slidesToScroll": 6,
        "arrows":true
        {rdelim}
        {else}
        "unslick"
        {/if}
      {rdelim},
      {ldelim}
        "breakpoint": 280,
        "settings":"unslick"
      {rdelim}
    ]{rdelim}{/strip}'>
    {categories nodes=$categories.children}
  </div>
</div>
{/if}