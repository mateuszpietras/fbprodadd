{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div class="user-info header__item">
  {if $logged}
    <a
      class="account header__link header__link--account"
      href="{$my_account_url}"
      title="{l s='View my customer account' d='Shop.Theme.Customeraccount'}"
      rel="nofollow"
    >
      <svg class="logged" width="28" height="28" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M21.125 22.75C21.125 22.75 22.75 22.75 22.75 21.125C22.75 19.5 21.125 14.625 13 14.625C4.875 14.625 3.25 19.5 3.25 21.125C3.25 22.75 4.875 22.75 4.875 22.75H21.125ZM4.88313 21.216V21.2127V21.216ZM4.91075 21.125H21.0892C21.0969 21.1241 21.1044 21.123 21.112 21.1217L21.125 21.1185C21.1234 20.7188 20.8748 19.5162 19.773 18.4145C18.7135 17.355 16.7196 16.25 13 16.25C9.27875 16.25 7.2865 17.355 6.227 18.4145C5.12525 19.5162 4.87825 20.7188 4.875 21.1185C4.88689 21.1208 4.89881 21.123 4.91075 21.125ZM21.1185 21.216V21.2127V21.216ZM13 11.375C13.862 11.375 14.6886 11.0326 15.2981 10.4231C15.9076 9.8136 16.25 8.98695 16.25 8.125C16.25 7.26305 15.9076 6.4364 15.2981 5.8269C14.6886 5.21741 13.862 4.875 13 4.875C12.138 4.875 11.3114 5.21741 10.7019 5.8269C10.0924 6.4364 9.75 7.26305 9.75 8.125C9.75 8.98695 10.0924 9.8136 10.7019 10.4231C11.3114 11.0326 12.138 11.375 13 11.375ZM17.875 8.125C17.875 9.41793 17.3614 10.6579 16.4471 11.5721C15.5329 12.4864 14.2929 13 13 13C11.7071 13 10.4671 12.4864 9.55285 11.5721C8.63861 10.6579 8.125 9.41793 8.125 8.125C8.125 6.83207 8.63861 5.59209 9.55285 4.67785C10.4671 3.76361 11.7071 3.25 13 3.25C14.2929 3.25 15.5329 3.76361 16.4471 4.67785C17.3614 5.59209 17.875 6.83207 17.875 8.125Z" fill="black"/>
      </svg>
      <small class="visible--desktop account__name">{$customerName}</small>
    </a>
  {else}
    <a
      href="{$my_account_url}"
      class="header__link"
      title="{l s='Log in to your customer account' d='Shop.Theme.Customeraccount'}"
      rel="nofollow"
    >
      <svg width="28" height="28" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M21.125 22.75C21.125 22.75 22.75 22.75 22.75 21.125C22.75 19.5 21.125 14.625 13 14.625C4.875 14.625 3.25 19.5 3.25 21.125C3.25 22.75 4.875 22.75 4.875 22.75H21.125ZM4.88313 21.216V21.2127V21.216ZM4.91075 21.125H21.0892C21.0969 21.1241 21.1044 21.123 21.112 21.1217L21.125 21.1185C21.1234 20.7188 20.8748 19.5162 19.773 18.4145C18.7135 17.355 16.7196 16.25 13 16.25C9.27875 16.25 7.2865 17.355 6.227 18.4145C5.12525 19.5162 4.87825 20.7188 4.875 21.1185C4.88689 21.1208 4.89881 21.123 4.91075 21.125ZM21.1185 21.216V21.2127V21.216ZM13 11.375C13.862 11.375 14.6886 11.0326 15.2981 10.4231C15.9076 9.8136 16.25 8.98695 16.25 8.125C16.25 7.26305 15.9076 6.4364 15.2981 5.8269C14.6886 5.21741 13.862 4.875 13 4.875C12.138 4.875 11.3114 5.21741 10.7019 5.8269C10.0924 6.4364 9.75 7.26305 9.75 8.125C9.75 8.98695 10.0924 9.8136 10.7019 10.4231C11.3114 11.0326 12.138 11.375 13 11.375ZM17.875 8.125C17.875 9.41793 17.3614 10.6579 16.4471 11.5721C15.5329 12.4864 14.2929 13 13 13C11.7071 13 10.4671 12.4864 9.55285 11.5721C8.63861 10.6579 8.125 9.41793 8.125 8.125C8.125 6.83207 8.63861 5.59209 9.55285 4.67785C10.4671 3.76361 11.7071 3.25 13 3.25C14.2929 3.25 15.5329 3.76361 16.4471 4.67785C17.3614 5.59209 17.875 6.83207 17.875 8.125Z" fill="black"/>
      </svg>
      {* <span class="visible--desktop small">{l s='Sign in' d='Shop.Theme.Actions'}</span> *}
    </a>
  {/if}
</div>
