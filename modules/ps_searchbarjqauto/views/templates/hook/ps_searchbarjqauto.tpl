{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div id='_desktop_search_wrapper'>
    <form  method="get" action="{$search_controller_url}" class="search-widget" data-search-widget
        data-search-controller-url="{$search_controller_url}">
        {* <input type="hidden" name="controller" value="search" class=""> *}
        <div class="search-widget__group">
            <label class="sr-only" for="searchInput">{l s='Search our catalog' d='Shop.Theme.Catalog'}</label>
            <input id="searchInput" class="search-widget__input form-control form-control-lg" required type="text" name="s" value="{$search_string}"
                placeholder="{l s='Search our catalog' d='Shop.Theme.Catalog'}"
                aria-label="{l s='Search' d='Shop.Theme.Catalog'}">
            <button type="submit" class="search-widget__btn ">
                <svg width="26" height="26" viewBox="0 0 26 26" fill="black" xmlns="http://www.w3.org/2000/svg"><path d="M24.375 23.2294L18.2406 17.0625C19.7071 15.293 20.4357 13.0267 20.2749 10.7341C20.1142 8.44156 19.0765 6.29904 17.3773 4.75157C15.6782 3.2041 13.4483 2.37062 11.1508 2.42424C8.85321 2.47787 6.66461 3.41448 5.03955 5.03954C3.41449 6.6646 2.47788 8.8532 2.42425 11.1508C2.37062 13.4483 3.20411 15.6782 4.75158 17.3773C6.29905 19.0765 8.44157 20.1142 10.7341 20.2749C13.0267 20.4357 15.2931 19.7071 17.0625 18.2406L23.2294 24.375L24.375 23.2294ZM4.06251 11.375C4.06251 9.92872 4.49138 8.51493 5.29489 7.31239C6.09839 6.10986 7.24045 5.1726 8.57664 4.61913C9.91282 4.06567 11.3831 3.92085 12.8016 4.20301C14.2201 4.48516 15.5231 5.18161 16.5457 6.20428C17.5684 7.22695 18.2648 8.52992 18.547 9.9484C18.8292 11.3669 18.6843 12.8372 18.1309 14.1734C17.5774 15.5096 16.6401 16.6516 15.4376 17.4551C14.2351 18.2586 12.8213 18.6875 11.375 18.6875C9.43561 18.6875 7.57565 17.9171 6.20429 16.5457C4.83293 15.1744 4.06251 13.3144 4.06251 11.375Z" fill="black"></path></svg>
                <span class="d-none ">{l s='Search' d='Shop.Theme.Catalog'}</span>
            </button>
        </div>
    </form>
</div>