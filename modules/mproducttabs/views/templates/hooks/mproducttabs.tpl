    {foreach from=$product_tabs item=tab}
        <div class="card card__special mb-2">
            <div class="card-header" id="tab_heading_{$tab.id|escape:'htmlall':'UTF-8'}">
                <button class="btn btn-link btn-block" data-toggle="collapse"
                    data-target="#tab_content_{$tab.id|escape:'htmlall':'UTF-8'}" aria-expanded="true"
                    aria-controls="tab_content_{$tab.id|escape:'htmlall':'UTF-8'}">
                    {$tab.name|escape:'htmlall':'UTF-8'}<i class="material-icons">expand_more</i>
                </button>
            </div>
            <div id="tab_content_{$tab.id|escape:'htmlall':'UTF-8'}" class="show collapse{if $tab.is_open} in{/if}"
                {if $tab.is_open} aria-expanded="true" {/if}
                aria-labelledby="tab_heading_{$tab.id|escape:'htmlall':'UTF-8'}" data-parent="#accordion">
                <div class="card-body product-tabs">
                    {$tab.content|unescape:"html"|replace:'<img src':'<img alt="detail" class="img-fluid lazyload" data-src'|replace:'<iframe':'<iframe title="film"'|replace:'frameborder="0"':'' nofilter}
                </div>
            </div>
        </div>
    {/foreach}