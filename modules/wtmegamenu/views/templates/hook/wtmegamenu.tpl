{**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- Module Megamenu-->
<nav id="_desktop_top_menu" class="menu visible--desktop">
{$id_lang = Context::getContext()->language->id}
	{* <div class="title-menu-mobile"><span>{l s='Navigation' mod='wtmegamenu'}</span></div> *}
		<ul class="menu-top menu-sub__list align-items-xl-center">
			{foreach from=$menus key=nb item=menu name=menus}
				{if isset($menu.type) && $menu.type == 'CAT' && $menu.dropdown == 1}
					{$menu.sub_menu|escape:'quotes':'UTF-8' nofilter}
				{else}		

					<li class="menu__item level-1 {$menu.class}{if count($menu.sub_menu) > 0} parent{/if} h-100" data-item-id="{$menu.id_wtmegamenu}">
						{if $menu.type_icon == 0 && $menu.icon != ''}
							<img class="img-icon" src="{$icon_path}{$menu.icon}" alt=""/>
						{elseif  $menu.type_icon == 1 && $menu.icon != ''}
							<i class="{$menu.icon}"></i>
						{/if}
							<div class="menu__item-header">
								<a href="{$menu.link}" class="d-md-flex w-100 h-100 menu__item-link--top {$menu.class} {if count($menu.sub_menu) > 0}menu__item-link--hassubmenu{else}menu__item-link--nosubmenu{/if}"><span class="align-self-center">{$menu.title}</span></a>						

								{if $menu.subtitle != ''}<span class="menu-subtitle">{$menu.subtitle}</span>{/if}

								{if count($menu.sub_menu) > 0} 
									{assign var=_expand_id value=10|mt_rand:100000}
									<span class="visible--mobile">
										<span data-target="#top_sub_menu_{$_expand_id}" data-toggle="collapse"
											class="d-block navbar-toggler icon-collapse">
										{* <i class="material-icons menu__collapseicon">&#xE313;</i> *}
										<div class="menu__collapseicon">
											<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-chevron-down" width="24" height="24" viewBox="0 0 24 24" stroke-width="1" stroke="#597e8d" fill="none" stroke-linecap="round" stroke-linejoin="round">
												<path stroke="none" d="M0 0h24v24H0z" fill="none"/>
												<polyline points="6 9 12 15 18 9" />
											</svg>
										</div>
										</span>
									</span>
								{/if}
							</div>
							{if isset($menu.sub_menu) && count($menu.sub_menu) > 0}
								<div {if count($menu.sub_menu) > 0} id="top_sub_menu_{$_expand_id}"{/if} class="menu-sub wt-sub-menu menu-dropdown collapse show" data-collapse-hide-mobile>
									{foreach from=$menu.sub_menu item= menu_row name=menu_row}
										<div class="wt-menu-row row {$menu_row.class}">
											{if isset($menu_row.list_col) && count($menu_row.list_col) > 0}
												{foreach from=$menu_row.list_col item= menu_col name=menu_col}
													<div class="wt-menu-col col-xs-12 {$menu_col.width} {$menu_col.class}">
														{if count($menu_col.list_menu_item) > 0}
															<ul class="ul-column px-3 p-md-3">
															{foreach from=$menu_col.list_menu_item item= sub_menu_item name=sub_menu_item}
																<li class="menu-item {if $sub_menu_item.type_item == 1} item-header{else} item-line{/if}">
																	{if $sub_menu_item.type_link == 3}
																		{if !empty($sub_menu_item.link)}<a class="menu__link" href="{$sub_menu_item.link}">{$sub_menu_item.title}</a>{/if}
																		<div class="html-block">
																			{$sub_menu_item.text|escape:'quotes':'UTF-8' nofilter}
																		</div>
																	{else}
																		{if !empty($sub_menu_item.link)}<a class="menu__link" href="{$sub_menu_item.link}">{$sub_menu_item.title}</a>{/if}
																	{/if}
																</li>
															{/foreach}
															</ul>
														{/if}
													</div>
												{/foreach}
											{/if}
										</div>
									{/foreach}
								</div>
							{/if}
						{if count($menu.sub_menu) > 0} 
						
						{/if}
					</li>
				{/if}
			{/foreach}
		</ul>
</nav>
<!-- /Module Megamenu -->