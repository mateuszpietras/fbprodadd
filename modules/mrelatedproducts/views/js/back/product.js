/**
* 2007-2021 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2021 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/

$(document).ready(function () {
    var type = $('#MRELATED_TYPE').val();
    switch(type) {
        case 'products':
            $('#related_category').fadeOut();
            $('#related_products').fadeIn();
        break;
        case 'category':
            $('#related_products').fadeOut();
            $('#related_category').fadeIn();
        break;
    }
});
					
$(document).on('change', '#MRELATED_TYPE', function() {
    var type = $(this).val();
    switch(type) {
        case 'products':
            $('#related_category').fadeOut();
            $('#related_products').fadeIn();
        break;
        case 'category':
            $('#related_products').fadeOut();
            $('#related_category').fadeIn();
        break;
    }
});

$(document).on('change', '[name="MRELATED_GLOBAL"]', function() {
    var checked = $(this).is(':checked');
    if(checked) {
        $('#related_global_categories').fadeIn();
        $('#MRELATED_CATEGORIES').attr('required','required');
    } else {
        $('#related_global_categories').fadeOut();
        $('#MRELATED_CATEGORIES').removeAttr('required');
    }
});

$(document).on('click', '.mrelatedproducts_inside_button', function(e) {
    e.preventDefault();
    var id_product = $('#form_id_product').val();    
    var method = $(this).data('method');
    var button_type = $(this).data('button-type');
    var token = $(this).data('token');
    var id_related = $(this).data('id-related');
    var formData = '';
    var button_action = '';
    var errors = '';

    if (method != undefined) { 

        if (button_type != undefined && button_type == 'templateChange') {
            button_action = '&templateChange=1';
        } else {
            formData = '&' + $('#related-form').find('input:not([type="checkbox"]), select').serialize();
            $('#related-form input[type=checkbox]').each(function() {     
                if (!this.checked) {
                    formData += '&'+this.name+'=0';
                } else {
                    formData += '&'+this.name+'=1';
                }
            });
        }

        if (id_related != undefined){
            button_action += '&id_related=' + id_related;
        }

        $.ajax({
            type: 'POST',
            url: baseDir + 'modules/mrelatedproducts/mrelatedproducts-ajax.php',
            data: '&id_product='+ id_product +'&ajax=1&key_tab=ModuleMrelatedproducts' + button_action + '&method=' + method + formData + '&token=' + token,
            dataType: 'json',
            success: function(result) {

                if (!result.hasError) {
                    if (result.content) {
                        $('#module_mrelatedproducts').fadeOut('fast').html(result.content).fadeIn('fast');
                        addTokenfieldToProducts();
                    }
                } else {
                    $.each(result.errors, function(key, error) {
                        errors +=  '<p>'+ (1 + key) + '. ' + error + '</p>';
                    });
                    $('#mrelatedproducts_errors').fadeOut('fast').fadeIn('fast').html(errors);
                }
            }
        });
    } else {
        console.error('"data-method" attribute was not found in the button element');
    }
});

function addTokenfieldToProducts () {
    if ($('#MRELATED_PRODUCTS').length) {
        $('#MRELATED_PRODUCTS').tokenfield({
            createTokensOnBlur: true,
        });
    }
    if ($('#MRELATED_CATEGORIES').length) {
        $('#MRELATED_CATEGORIES').tokenfield({
            createTokensOnBlur: true,
        });
    }
}