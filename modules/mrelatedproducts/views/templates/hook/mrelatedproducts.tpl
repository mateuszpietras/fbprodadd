{*
* 2007-2021 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2021 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{foreach $related as $relate}
    {if $relate.active}
    <div class="page-product-box mrelatedproducts" id="mrelatedproducts_{$relate.id_related|escape:'html':'UTF-8'}">
        {block name='product_list_header'}
          <h3 id="js-product-list-header" class="display-2">{$relate.name|escape:'html':'UTF-8'}</h3>
        {/block}

        {if $relate.products}
        <div id="js-product-list">
            {assign var="relatedproductscount" value=$relate.products|count}
            <div class="products products-slick spacing-md-top{if $relatedproductscount > 1} products--slickmobile{/if}" data-slick='{strip}
                {ldelim}
                "slidesToShow": 1,
                "slidesToScroll": 1,
                "mobileFirst":true,
                "arrows":true,
                "rows":0,
                "responsive": [
                  {ldelim}
                    "breakpoint": 992,
                    "settings":
                    {if $relatedproductscount > 4}
                    {ldelim}
                    "arrows":true,
                    "slidesToShow": 4,
                    "slidesToScroll": 4,
                    "arrows":true
                    {rdelim}
                    {else}
                    "unslick"
                    {/if}
                  {rdelim},
                  {ldelim}
                    "breakpoint": 720,
                    "settings":
                    {if $relatedproductscount > 3}
                    {ldelim}
                    "arrows":true,
                    "slidesToShow": 3,
                    "slidesToScroll": 3
                    {rdelim}
                    {else}
                    "unslick"
                    {/if}
                  {rdelim},
                  {ldelim}
                    "breakpoint": 540,
                    "settings":
                    {if $relatedproductscount > 2}
                    {ldelim}
                    "arrows":true,
                    "slidesToShow": 2,
                    "slidesToScroll": 2
                    {rdelim}
                    {else}
                    "unslick"
                    {/if}
                  {rdelim}
                ]{rdelim}{/strip}'>    
                {foreach from=$relate.products item="product"}
                  {block name='product_miniature'}
                    {include file='catalog/_partials/miniatures/product.tpl' product=$product}
                  {/block}
                {/foreach}
            </div>
        </div>
        {else}
        <div class="alert alert-info">{l s='No product at this time.' mod='mrelatedproducts'}</div>
        {/if}
    </div>
    {/if}
{/foreach}
