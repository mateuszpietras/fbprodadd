{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if isset($htmlitems) && $htmlitems}
    <section class="themeconfig mb-lg-5 d-none d-lg-block d-xl-block">
        {if $hook == 'home'}
            <ul class="themeconfig__home{if $hook == 'home'} row{/if}">
                {foreach name=items from=$htmlitems item=hItem}
                    <li class="themeconfig__item themeconfig__item--{$hook}{if $hook == 'home'} col-12 col-sm-6 col-md-3{/if}">
                        {if $hItem.url}
                            <a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="themeconfig__link" {if $hItem.target == 1}
                                onclick="return !window.open(this.href);" {/if} title="{$hItem.title|escape:'htmlall':'UTF-8'}">
                            {/if}
                            {if $hItem.image}
                                <div class="rc ratio2_1 rc--lazyload">
                                    <img data-src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="themeconfig__img img-fluid lazyload"
                                        title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}"
                                        width="{if $hItem.image_w}{$hItem.image_w|intval}{else}100%{/if}"
                                        height="{if $hItem.image_h}{$hItem.image_h|intval}{else}100%{/if}" 
                                    />
                                </div>
                            {/if}
                            {if $hItem.title && $hItem.title_use == 1}
                                <h3 class="themeconfig__title">{$hItem.title|escape:'htmlall':'UTF-8'}</h3>
                            {/if}
                            {if $hItem.html}
                                <div class="themeconfig__content">
                                    {$hItem.html}
                                </div>
                            {/if}
                            {if $hItem.url}
                            </a>
                        {/if}
                    </li>
                {/foreach}
            </ul>
        {/if}
        {if $hook == 'footer'}
            {foreach name=items from=$htmlitems item=hItem}
                <figure class="themeconfig__footer lazyload"{if $hItem.image} data-bg="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}"{/if}>
                    {if $hItem.title && $hItem.title_use == 1}
                        <h3 class="themeconfig__title">{$hItem.title|escape:'htmlall':'UTF-8'}</h3>
                    {/if}
                    <div class="themeconfig__content">
                        {$hItem.html nofilter}
                    </div>
                </figure>
            {/foreach}
        {/if}
        {if $hook == 'left' && !Context::getContext()->isMobile() && !Context::getContext()->isTablet()}
            {foreach name=items from=$htmlitems item=hItem}
                {if $hItem.url}
                <a class="themeconfig__link d-block rc mb-3" href="{$hItem.url}" style="padding-top: {if $hItem.image_w && $hItem.image_h}{($hItem.image_h/$hItem.image_w)*100}{else}100{/if}%;">
                {else}
                    <div class="themeconfig__banner rc mb-3" style="padding-top: {if $hItem.image_w && $hItem.image_h}{($hItem.image_h/$hItem.image_w)*100}{else}100{/if}%;">
                {/if}
                    <img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="themeconfig__img img-fluid lazyload"
                        title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}"
                        width="{if $hItem.image_w}{$hItem.image_w|intval}{else}100%{/if}"
                        height="{if $hItem.image_h}{$hItem.image_h|intval}{else}100%{/if}" />
                {if $hItem.url}
                    </a>
                {else}
                    </div>
                {/if}
            {/foreach}
        {/if}
    </section>

{/if}