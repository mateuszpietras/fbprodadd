{foreach from=$subcategories item=$category}
    {if ($category['id_category_parent'] == $id_category)}
        {$is_subcategories = true}
    {else}
        {$is_subcategories = false}
    {/if}
{/foreach}
{if $is_subcategories}
<div class="d-lg-none d-xl-none">
    <button type="button" id="subcategories_toggle" data-target="#offcanvas_subcategories" data-toggle="modal" class="btn btn-secondary">{l s='Subcategories' d='Shop.Theme.Global'}</button> 
</div>

<div id="_desktop_subcategories" class="block-categories visible--desktop">
    <section class="categories msubcategories-slick {if $number_attached_subcategories > 1} msubcateogires--slickmobile{/if}" data-slick='{strip}
        {ldelim}
        "slidesToShow": 1,
        "slidesToScroll": 1,
        "mobileFirst":true,
        "arrows":true,
        "rows":0,
        "responsive": [
            {ldelim}
            "breakpoint": 992,
            "settings":
            {if $number_attached_subcategories > 4}
            {ldelim}
            "arrows":true,
            "slidesToShow": 6,
            "slidesToScroll": 6,
            "arrows":true
            {rdelim}
            {else}
            "unslick"
            {/if}
            {rdelim},
            {ldelim}
            "breakpoint": 280,
            "settings": "unslick"
            {rdelim}
        ]
    {rdelim}{/strip}'>
        {foreach from=$subcategories item=$category}
            {if ($category['id_category_parent'] == $id_category)}
            <article class="box">
                <a class="box__wrapper" href={$category['url_category']}>
                    <div class="box__thumbnail rc ratio1_1">
                        <img class="img-fluid lazyload" data-src={$category['image']}>
                    </div>
                    <h3 class="box__title">{$category['name']}</h3>
                </a>
            </article>
            {/if}
        {/foreach}
    </section>
</div>
{/if}