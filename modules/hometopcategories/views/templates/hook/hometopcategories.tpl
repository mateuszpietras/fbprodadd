{if !$isMobile}
<section class="top-categories">
	<h3 class="display-2 text-center">{l s="Which room do you want to furnish?" d='Modules.Hometopcategories.Hometopcategories'}</h3>
	<div class="top-categories__wrapper row justify-content-center">
		{foreach from=$top_categories item=category key=key}
		<article class="box col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
			<a class="box__wrapper" href="{$link->getCategoryLink($category.id_category, $category.link_rewrite)|escape:'html':'UTF-8'}">
				<div class="box__thumbnail rc ratio1_1">
					<img 
						class="img-responsive lazyload" 
						data-src="{$link->getCatImageLink($category.link_rewrite, $category.id_category, 'category_default')|escape:'html':'UTF-8'}" 
						alt="{$category.name|escape:'html':'UTF-8'}" 
						width="200" 
						height="200"
					/>
				</div>
				<h3 class="box__title">{$category.name|escape:'html':'UTF-8'}</h3>
			</a>
		</article>
		{/foreach}
	</div>
</section>
{/if}
