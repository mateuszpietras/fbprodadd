{extends file=$layout}
{block name='content'}
    <h1 class="display-2">{l s='My wishlist' mod='mwishlist'}</h1>
    <div id="my-wishlist" class="my-wishlist">
        {block name='wishlist_content'}
            {include file='module:mwishlist/views/templates/front/_partials/content.tpl' products=$products}
        {/block}
    <div>
{/block}