{if !$products}

    <aside class="alert alert-secondary">
        <h3 class="h3 mb-1">{l s='Your wishlist will live here' mod='mwishlist'}</h3>
        <p class="mb-0">{l s='Explore our shop and add your favorite products. ' mod='mwishlist'}</p>
        {if $exploreBtn && $exploteLink}
            <a href="{$exploteLink}" class="btn btn-primary mt-2">{l s='Start exploring' mod='mwishlist'}</a>
        {/if}
    </aside>
{else}
    <div class="wishlist__loader">
        <div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
    </div>
    <section class="wishlist__products row">
        {foreach from=$products item=product}
            <article class="wishlist-product col-12 col-md-4 col-lg-3">
                <div class="wishlist-product__wrapper">
                    <div class="wishlist-product__image">
                        <button class="wishlist-product__remove" data-id-product="{$product.id_product}">
                            <span class="material-icons">delete</span>
                        </button>
                        {block name='product_thumbnail'}
                            <a href="{$product.url}" class="rc ratio1_1">
                                {if $product.cover}
                                    <img data-src="{$product.cover.bySize.home_default.url}"
                                        alt="{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
                                        data-full-size-image-url="{$product.cover.large.url}" class="lazyload">
                                {elseif isset($urls.no_picture_image)}
                                    <img class="lazyload" src="{$urls.no_picture_image.bySize.home_default.url}">
                                {else}
                                    <img class="lazyload"
                                        src="data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==">
                                {/if}
                            </a>
                        {/block}
                    </div>
                    <div class="wishlist-product__content">
                        {block name='product_name'}
                            <h3 class="h3 product-title"><a href="{$product.url}">{$product.name|truncate:30:'...'}</a></h3>
                        {/block}
                        {block name='product_reviews'}
                            {hook h='displayProductListReviews' product=$product}
                        {/block}
                        {block name='product_price_and_shipping'}
                            {if $product.show_price}
                                <div class="product-price-and-shipping text-center">
                                    {if $product.has_discount}
                                        {hook h='displayProductPriceBlock' product=$product type="old_price"}
                                        <span class="sr-only">{l s='Regular price' d='Shop.Theme.Catalog'}</span>
                                        <span class="regular-price">{$product.regular_price}</span>
                                    {/if}
                                    {hook h='displayProductPriceBlock' product=$product type="before_price"}
                                    <span class="sr-only">{l s='Price' d='Shop.Theme.Catalog'}</span>
                                    <span class="price{if $product.has_discount} current-price-discount{/if}">{$product.price}</span>
                                    {hook h='displayProductPriceBlock' product=$product type='unit_price'}
                                    {hook h='displayProductPriceBlock' product=$product type='weight'}
                                    {hook h='displayProductCombinationSupplier' product=$product type="list"}
                                </div>
                            {/if}
                        {/block}
                        <form action="{$urls.pages.cart}" method="post" class="mt-2">
                            <input type="hidden" name="token" value="{$static_token}">
                            <input type="hidden" value="{$product.id_product}" name="id_product">
                            <div class="input-group mb-3">
                                <input type="number" class="input-group form-control" min="{$product.minimal_quantity}"
                                    value="{$product.minimal_quantity}" name="qty" />
                                <div class="input-group-append">
                                    <button data-button-action="add-to-cart"
                                        class="btn btn-primary">{l s='Add to cart' mod='mwishlist'}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </article>
        {/foreach}
    </section>
{/if}