<div class="col-12 mb-4">
<div class="card p-3 p-md-4 p-xl-5">
  <div class="flex-row row align-items-center">
      <div class="col-12 col-md-4 col-xl-3 mb-4 mb-md-0">
        <h2><b>{l s='Your wishlist' mod='mwishlist'}</b></h2>
        <h5>{l s='Create a wishlist that will help you remember the products you like and come back to them at any time.' mod='mwishlist'}</h5>
    </div>
    <div class="col-12 col-md-8 col-xl-9 mb-md-0">
        <h4><b>{l s='Go to your wishlist' mod='mwishlist'}</b></h4>
        <a class="btn btn-primary" id="order-slips-link" href="{$link->getModuleLink('mwishlist','wishlist')}" title="{l s='Your wishlist' mod='mwishlist'}">
        {l s='Your wishlist' mod='mwishlist'}
        </a>
        </div>
        </div>
    </div>
</div>