{assign var="products_nb" value=$wishlist->getProductNb()}
{assign var="wishlist_url" value=$link->getModuleLink('mwishlist','wishlist')}

<div class="wishlist">
    <a class="header__link wishlist__link" href="{$wishlist_url}">
        <svg class="wishlist__icon" width="26" height="26" viewBox="0 0 26 26" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <g clip-path="url(#clip0)">
                <path
                    d="M12.9999 4.46553L11.8348 3.26791C9.09994 0.456658 4.08519 1.42678 2.27494 4.96116C1.42506 6.62353 1.23331 9.02366 2.78519 12.0868C4.28019 15.0362 7.39044 18.5689 12.9999 22.4169C18.6094 18.5689 21.7181 15.0362 23.2147 12.0868C24.7666 9.02203 24.5764 6.62353 23.7249 4.96116C21.9147 1.42678 16.8999 0.455033 14.1651 3.26628L12.9999 4.46553ZM12.9999 24.375C-11.9162 7.91053 5.32831 -4.93997 12.7139 1.85741C12.8114 1.94678 12.9073 2.03941 12.9999 2.13528C13.0916 2.0395 13.187 1.94734 13.2859 1.85903C20.6699 -4.94322 37.9161 7.90891 12.9999 24.375Z"
                    fill="black" />
            </g>
            <defs>
                <clipPath id="clip0">
                    <rect width="26" height="26" fill="white" />
                </clipPath>
            </defs>
        </svg>

        <small class="header__pill wishlist__counter{if !$products_nb} wishlist__counter--unvisble{/if}">
            {$products_nb}
        </small>
    </a>
</div>

<script>
    var wishlist_url = "{$wishlist_url}";
</script>