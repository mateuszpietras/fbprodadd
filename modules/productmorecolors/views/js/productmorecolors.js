var cachedProductColors = [];


$(document).on('mouseenter', '.js-product-miniature', function(e) {
    var wrapper = $(this).find('.productmorecolors')[0];
    let is_cached = false

    if(cachedProductColors && cachedProductColors.length)
        cachedProductColors.forEach(id_product => {
            if(id_product == wrapper.dataset.idProduct)
                is_cached = true
        })

    if(!is_cached)
        new Colors(wrapper)

});

var Colors = function(el) {
    this.el = el;
    this.id_product = parseInt(this.el.dataset.idProduct);
    this.groups = false;
    this.link = '';

    this.init();
}

Colors.prototype.init = async function() {
    //this.removeColorsLists();
    cachedProductColors.push(this.id_product)

    if(!this.groups)
        var data = await this.getGroups();

    this.groups = Object.values(data.data.groups.groups);
    this.combinations = Object.values(data.data.groups.combinations);
    this.link = data.data.groups.product_link;

    this.render();
}


Colors.prototype.removeColorsLists = function() {
    var attributes_lists = document.querySelectorAll('.productmorecolors__attributes');

    attributes_lists.forEach(function(attributes_list) {
        attributes_list.remove();
    });
};

Colors.prototype.render = function() {
    var isLoaded = false
    var wrapper = document.createElement('div');
    wrapper.classList.add('productmorecolors__attributes p-1');

    this.el.querySelector('.productmorecolors__label').classList.add('d-none')
    // this.el.children()
    // var closeBtn = document.createElement('button');
    // closeBtn.classList.add('productmorecolors__close');
    // closeBtn.addEventListener('click', function() {
    //     wrapper.parentNode.removeChild(wrapper);
    // });
  
    // wrapper.append(closeBtn);

    var product_image = this.el.parentNode.parentNode.querySelector('img');
    var selected_attribute = product_image.getAttribute('data-id-attribute');

    this.groups.forEach(group => {
        var limit = 6
       
       if(group.group_type = "color" && group.attributes.length > 1) {
           isLoaded = true;

            group.attributes.forEach((attribute, key) => {
                if(key <= limit){
                    var image = document.createElement('img');
                    image.src = '/img/co/' + attribute.id + '.jpg';
                    image.classList.add('productmorecolors__attribute');

                    if(group.default == attribute.id && !selected_attribute) {
                        image.classList.add('productmorecolors__attribute--selected');
                    }
                    if(selected_attribute) {
                        if(selected_attribute == attribute.id) {
                            image.classList.add('productmorecolors__attribute--selected');
                        }
                    } 
                    image.setAttribute('data-id-attribute', attribute.id);
                    image.width = 25;
                    image.height = 25;
                    var _this = this;

                    image.addEventListener('click', function(e) {
                        
                        var attributes = wrapper.querySelectorAll('.productmorecolors__attribute');

                        attributes.forEach(function(attribute) {
                            attribute.classList.remove('productmorecolors__attribute--selected')
                        });
                        
                        var id_attribute = parseInt(e.target.dataset.idAttribute); 
                        var image_link = _this.findImage(id_attribute);
                        this.classList.add('productmorecolors__attribute--selected');
                        var image = _this.el.parentNode.parentNode.querySelector('img');
                        image.setAttribute('data-id-attribute', id_attribute);
                        image.src = image_link;
                    });

                    wrapper.append(image);

                    if(key == limit && group.attributes.length - (key + 1) > 0) {
                        var counter = group.attributes.length - (key + 1);
                        var link = document.createElement('a');
                        link.href = this.link;
                        link.textContent = ' + '+ counter
                        wrapper.append(link)
                    }
                }

            })
       }

    }); 

    if(isLoaded){
        this.el.parentNode.append(wrapper);
        wrapper.classList.add('attribibutes__visible');
    }
}

Colors.prototype.getGroups = function() {

    return $.ajax({
        type: 'GET',
        url:  location.origin + '/modules/productmorecolors/productmorecolors-ajax.php',
        data: {
            'method': 'productColors',
            'id_product': this.id_product
        },
        dataType: 'json',
      });
}

Colors.prototype.findImage = function(id_attribute) {

    var image_link = false;

    this.combinations.forEach(function(combination) {
        
        if(combination.attributes.includes(id_attribute)) {
            image_link = combination.image_link;
        }
    });

    return image_link;
}