import prestashop from 'prestashop';

if('serviceWorker' in navigator) {
    if (navigator.serviceWorker.controller) {
        console.log('[PWA Builder] active service worker found, no need to register');
    } else {
        //Register the ServiceWorker
        navigator.serviceWorker.register(`${prestashop.urls.base_url}sw.js`, {
            scope: `/`
        }).then(reg => {
            console.log('Service worker has been registered for scope: ' + reg.scope);
        }).catch(e => {
            console.log('Service worker has been not registered. ' + e);
        });
    }
// navigator.serviceWorker.getRegistrations().then(function (registrations) {
//     var _iteratorNormalCompletion = true;
//     var _didIteratorError = false;
//     var _iteratorError = undefined;

//     try {
//         for (var _iterator = registrations[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
//             var registration = _step.value;

//             if (registration.active) {
//                 if (registration.active.scriptURL == `${prestashop.urls.js_url}sw.js`) {
//                     registration.unregister();
//                 }
//             }
//         }
//     } catch (err) {
//         _didIteratorError = true;
//         _iteratorError = err;
//     } finally {
//         try {
//             if (!_iteratorNormalCompletion && _iterator.return != null) {
//                 _iterator.return();
//             }
//         } finally {
//             if (_didIteratorError) {
//                 throw _iteratorError;
//             }
//         }
//     }
// });
}



// navigator.serviceWorker.getRegistrations().then(function (registrations) {
//     var _iteratorNormalCompletion = true;
//     var _didIteratorError = false;
//     var _iteratorError = undefined;

//     try {
//         for (var _iterator = registrations[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
//             var registration = _step.value;

//             if (registration.active) {
//                 if (registration.active.scriptURL == `${prestashop.urls.js_url}sw.js`) {
//                     registration.unregister();
//                 }
//             }
//         }
//     } catch (err) {
//         _didIteratorError = true;
//         _iteratorError = err;
//     } finally {
//         try {
//             if (!_iteratorNormalCompletion && _iterator.return != null) {
//                 _iterator.return();
//             }
//         } finally {
//             if (_didIteratorError) {
//                 throw _iteratorError;
//             }
//         }
//     }
// });
