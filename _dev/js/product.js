/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
import $ from 'jquery';
import prestashop from 'prestashop';
import SlickSlider from './components/slick';
import '@fancyapps/fancybox';

$(document).ready(function () {
  createProductSpin();
  createInputFile();
  runTooltip();
  runTabsImages();

  const imgHeight = $('.product-cover > img').height();
  switchCover(imgHeight);
  let slickSlider = new SlickSlider();

  prestashop.on('updatedProduct', function (event) {
    createInputFile();
    switchCover(imgHeight);
    runTooltip()

    if (event && event.product_minimal_quantity) {
      const minimalProductQuantity = parseInt(event.product_minimal_quantity, 10);
      const quantityInputSelector = '#quantity_wanted';
      let quantityInput = $(quantityInputSelector);

      // @see http://www.virtuosoft.eu/code/bootstrap-touchspin/ about Bootstrap TouchSpin
      quantityInput.trigger('touchspin.updatesettings', { min: minimalProductQuantity });
    }
    $($('.tabs .nav-link.active').attr('href')).addClass('active').removeClass('fade');
    $('.js-product-images-modal').replaceWith(event.product_images_modal);
    slickSlider.init();
    
  });

  prestashop.on('responsive update', function (event) {
    switchCover(imgHeight);
  });

  $(document).on('click', '.texture', function(e) {
    runTooltip()
  })

  prestashop.on('updateProduct', function (event) {
    runTooltip()
  });

  function createInputFile() {
    $('.js-file-input').on('change', (event) => {
      let target, file;

      if ((target = $(event.currentTarget)[0]) && (file = target.files[0])) {
        $(target).prev().text(file.name);
      }
    });
  }

  function createProductSpin() {
    const $quantityInput = $('#quantity_wanted');

    $quantityInput.TouchSpin({
      buttondown_class: 'btn js-touchspin',
      buttonup_class: 'btn js-touchspin',
      min: parseInt($quantityInput.attr('min'), 10),
      max: 1000000
    });

    $('body').on('change keyup', '#quantity_wanted', (e) => {
      $(e.currentTarget).trigger('touchspin.stopspin');
      prestashop.emit('updateProduct', {
        eventType: 'updatedProductQuantity',
        event: e
      });
    });

  }

  function switchCover(imgHeight)
  {
    const thumbItems = document.querySelectorAll('.js-thumb-container');
    const thumbsContainer = $('.product-images');
    const zoomBtn = document.querySelector('.btn__zoom');
    
    if($(window).width() >= 768)
      thumbsContainer.css('height', imgHeight)
    
      
    thumbItems.forEach(thumbElement => {

      thumbElement.addEventListener('click', (e) => {
        const coverImg = document.querySelector('.product-cover > img');
        const current_id = coverImg.getAttribute('data-img');
        const thumbImage = thumbElement.querySelector('.js-thumb');
        const largeImgLink = thumbImage.getAttribute('data-image-large-src');
        const new_id = thumbImage.getAttribute('data-img');

        replaceCoverLinks(coverImg, current_id, new_id);
        zoomBtn.href = largeImgLink
        coverImg.setAttribute('data-img', new_id)

        resetSelectedThumbs(thumbItems)
        thumbImage.classList.add('js-thumb-selected')
        thumbImage.parentNode.classList.add('thumb-container__wrapper--selected')
      })
      
    });

  }

  function resetSelectedThumbs(items)
  {
    items.forEach(item => {
      const img = item.querySelector('.js-thumb');
      img.classList.remove('js-thumb-selected')
      img.parentNode.classList.remove('thumb-container__wrapper--selected')       
    });
    $('.js-webrotate').removeClass('thumb-container__wrapper--selected')
    $('.webrotate__box').removeClass('webrotate__box--active')
  }

  function replaceCoverLinks(img, old_id, new_id) 
  {
    let dataSrc = img.getAttribute('data-src');
    let srcset = img.getAttribute('srcset');
    let src = img.getAttribute('src');

    if(dataSrc) {
      dataSrc = dataSrc.replace('/'+ old_id + '-', '/' + new_id + '-') 
      img.setAttribute('data-src', dataSrc)
    }

    if(srcset) {
      srcset = srcset.replaceAll('/'+ old_id + '-', '/' + new_id + '-') 
      img.setAttribute('srcset', srcset)
    }

    if(src) {
      src = src.replace('/'+ old_id + '-', '/' + new_id + '-') 
      img.setAttribute('src', src)
    }

  }
  $('[data-fancybox="gallery"]').fancybox();
});


function runTabsImages(){
  const links = document.querySelectorAll('.card__special [data-fancybox]');

  if(links && links.length)
      links.forEach(item => {
          const parent = item.parentNode;
          const img = item.querySelector('img')
          if(img) {
            let href = item.href;

            if(href && href.includes('_width-')){
                let div = document.createElement('div');
                href = href.replace(/(_width-\d*)/, '');
                img.setAttribute('width', '');
                div.setAttribute('data-src', href);
                div.setAttribute('data-fancybox', 'gallery');
                div.setAttribute('data-type', 'image');
                div.classList.add('product__details');
                div.appendChild(img);
                
                parent.innerHTML = '';
                parent.appendChild(div);
  
            }
          }          
      })
}



// $(document).on('shown.bs.modal','#product-modal', function (e) {
//     $('#js-slick-product').resize();
// });

// //add to cart loader
// $(document).on('click','.js-add-to-cart:enabled:not(.is--loading)',function(){
//     $(this).addClass('is--loading').attr("disabled", true);
// });
// prestashop.on('updateCart', function (event) {
//     removeAddToCartLoader();

// });
// prestashop.on('handleError', function (event) {
//     removeAddToCartLoader();
//     $('.js-add-to-cart').attr("disabled", false);

// });
// function removeAddToCartLoader(){
//     $('.js-add-to-cart.is--loading').removeClass('is--loading');

// }


function runTooltip() {
  // hide all visible tooltips
  $('.tooltip').tooltip('hide');
  $('[data-toggle="tooltip"]').tooltip()
}