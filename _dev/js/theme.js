/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */


import 'bootstrap/js/src/util';
import 'bootstrap/js/src/alert';
// import 'bootstrap/js/src/button';
import '../node_modules/bootstrap/js/src/collapse.js';
// import '../node_modules/jquery-mask-plugin/src/jquery.mask'

import 'bootstrap/js/src/collapse';
import 'bootstrap/js/src/dropdown';
import 'bootstrap/js/src/modal';
// import 'bootstrap/js/src/popover';
import 'bootstrap/js/src/tab';
// import 'bootstrap/js/src/toast';
import 'bootstrap/js/src/tooltip';
import 'bootstrap-touchspin';

import './lib/slick.min';
import './lib/jquery.hoverIntent.min';
import SlickSlider from './components/slick';

import './responsive';
import './checkout';
import './customer';
import './listing';
import './product';
import './cart';

import './pwa';

import Form from './components/form';
// import TopMenu from './components/top-menu';

import prestashop from 'prestashop';
import EventEmitter from 'events';


import './components/block-cart';
import './components/wishlist';
// import lazysizes from 'lazysizes'
import 'lazysizes';
import 'default-passive-events';

import Vue from 'vue';
import Fabrics from './components/Fabrics/Fabrics.vue';
// import ShoppingCart from './components/ShoppingCart/ShoppingCart.vue'
// Vue.component('shopping-cart', ShoppingCart)
Vue.component('fabrics', Fabrics)

// "inherit" EventEmitter
for (var i in EventEmitter.prototype) {
  prestashop[i] = EventEmitter.prototype[i];
}

$(document).ready(() => {
  const form = new Form();
  let slickSlider = new SlickSlider();
  // let topMenuEl = $('#_desktop_top_menu #top-menu');
  // let topMenu = new TopMenu(topMenuEl);

  form.init();
  slickSlider.init();
  // topMenu.init();
  stickyMenu();

  //display input file content in custom file input BS
  $('.custom-file-input').on('change', function () {
    let fileName = $(this).val().split('\\').pop();
    $(this).next('.custom-file-label').addClass("selected").html(fileName);
  })

  $('.header__link--top').on('click', function(){
      $('.header__navbar--sticky').toggleClass('header__navbar--active')
  })

});

const lazySizesConfig = lazySizes.cfg;
lazySizesConfig.init = false;

window.addEventListener('load', lazySizes.init);

document.addEventListener('lazyloaded', function (e) {
  $(e.target).parent().addClass('rc--lazyload');
});

document.addEventListener('lazybeforeunveil', function (e) {
  var bg = e.target.getAttribute('data-bg');
  if (bg) {
    e.target.style.backgroundImage = 'url(' + bg + ')';
  }
});

window.addEventListener('DOMContentLoaded', (event) => {
  window.app = new Vue({
    el: '#app'
  });
});

document.addEventListener('animationend', (e) => {
  const { target, animationName } = e;

  if(animationName == 'pulse') {
    target.classList.remove('header__pill--animated');
  }
});


const stickyMenu = function(){
  const allowed_controllers = ['category', 'new-products', 'search', 'index', 'module-pm_advancedsearch4-searchresults'];
  const height_menu = $('.header__top').outerHeight();
  const IS_ALLOWED_CONTROLLER = allowed_controllers.includes(prestashop.page.page_name) ? true : false;
  const START_STICKY = 38;  //init header_top height - height of sticky menu (111 - 73)
  let IS_STICKY = false;
  
  if(IS_ALLOWED_CONTROLLER && window.innerWidth > 992){
    $(window).scroll(function() 
    {
      // var height_nav = $('.header__nav').outerHeight();
      // var height_navbar = $('.header__navbar').outerHeight();
  
      if ($(this).scrollTop() > START_STICKY && (!IS_STICKY || IS_STICKY == false))
      {
          IS_STICKY = true;
          $('.header__wrapper').addClass("header__wrapper--sticky");
          $('.header__top').addClass("header__top--sticky");
          $('.header__logo').addClass("header__logo--sticky");
          $('.header__nav').css('margin-bottom', height_menu)
          // $('.header__navbar').addClass("header__navbar--sticky");
          // $('.header__link--top').removeClass("visible--mobile").show().removeAttr('data-toggle');
      }
      else if($(this).scrollTop() <= START_STICKY)
      {
        if(IS_STICKY && IS_STICKY == true){
          $('.header__nav').css('margin-bottom', 0)
          $('.header__wrapper').removeClass("header__wrapper--sticky");
          $('.header__top').removeClass("header__top--sticky");
          $('.header__logo').removeClass("header__logo--sticky");
          IS_STICKY = false;
          // $('.header__navbar').removeClass("header__navbar--sticky header__navbar--active");
          // $('.header__link--top').addClass("visible--mobile").hide();
        }
      }
      
    });
  }
}

