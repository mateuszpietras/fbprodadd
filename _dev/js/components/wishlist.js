window.wishlist = {};

$(document).ready(function () {
    window.wishlist = new Wishlist('wishlist__icon', 'wishlist__counter');
});

$(document).on('click', '.wishlist-button', function () {
    var id_product = $(this).attr('data-id-product');

    window.wishlist.toggle(id_product, $(this));
});

$(document).on('click', '.wishlist-product__remove', function () {
    var id_product = $(this).attr('data-id-product');
    window.wishlist.remove(id_product, $(this));
});

$(document).on('click', '.wishlist-product__add-to-cart', function () {
    var id_product = $(this).attr('data-id-product');
    window.wishlist.addToCart(id_product);
});

$(document).on('click', '.wishlist-product__all-to-cart', function () {
    $('.wishlist-product__add-to-cart').each(function () {
        var id_product = $(this).attr('data-id-product');
        window.wishlist.addToCart(id_product, true);
    });
});

$(document).on('submit', '#sendWishlist', function (e) {
    e.preventDefault();
    var email = $('#customer_email').val();
    window.wishlist.sendToEmail(email);
});

class Wishlist {
    constructor(iconClass, counterClass) {
        this.productsNb = 0;
        this.counter = $('.' + counterClass);
        this.icon = $('.' + iconClass);
    }

    add(id_product) {
        let _this = this;

        $.ajax({
            type: 'POST',
            url: wishlist_url,
            data: {
                'ajax': 1,
                'id_product': id_product,
                'action': 'addproduct'
            },
            dataType: 'json',
            success: function (json) {
                if (!json.hasError) {
                    _this.productsNb = json.wishlist.productsNb;
                    _this.refresh();
                }
            }
        });
    }

    remove(id_product, el) {
        let _this = this;
        $('#my-wishlist').addClass('my-wishlist--loading')
        $.ajax({
            type: 'POST',
            url: wishlist_url,
            data: {
                'ajax': 1,
                'id_product': id_product,
                'action': 'removeproduct'
            },
            dataType: 'json',
            success: function (json) {
                if (!json.hasError) {
                    _this.productsNb = json.wishlist.productsNb;
                    _this.refresh();
                }
                $('#my-wishlist').html(json.content).removeClass('my-wishlist--loading')
            }
        });
    }

    toggle(id_product, el) {
        let _this = this;
        el.toggleClass('wishlist-button--liked');
        $.ajax({
            type: 'POST',
            url: wishlist_url,
            data: {
                'ajax': 1,
                'id_product': id_product,
                'action': 'toggleproduct'
            },
            dataType: 'json',
            success: function (json) {
                if (!json.hasError) {
                    _this.productsNb = json.wishlist.productsNb;
                    _this.refresh();
                    if (el) {
                        $('#wishlist_info_' + id_product).text(json.msg);
                    }
                } else {
                    el.toggleClass('wishlist__button--liked');
                }
            }
        });
    }

    setProductAsAddedToCart(id_product, el) {
        let _this = this;
        $('#my-wishlist').addClass('my-wishlist--loading')
        $.ajax({
            type: 'POST',
            url: wishlist_url,
            data: {
                'ajax': 1,
                'id_product': id_product,
                'action': 'SetProductAsAddedToCart'
            },
            dataType: 'json',
            success: function (json) {
                if (el) {
                    _this.productsNb = json.wishlist.productsNb;
                    _this.refresh();
                }
                $('#my-wishlist').html(json.content).removeClass('my-wishlist--loading')
            }
        });
    }

    refresh() {
        if(this.productsNb) {
            this.counter.removeClass('wishlist__counter--unvisble')
            this.counter.addClass('header__pill--animated')
        } else {
            this.counter.addClass('wishlist__counter--unvisble')
        }
        this.counter.text(this.productsNb);
    }

    sendToEmail(email = false) {
        let _this = this;
        
        var sendBtn = $('#sendWishlistButton');
        var alert = $('#wishlistAlert');
        sendBtn.attr('disabled', 'disabled');

        if (!email) {
            $.ajax({
                type: 'POST',
                url: wishlist_url,
                data: {
                    'ajax': 1,
                    'email': email,
                    'action': 'sendtoemail'
                },
                dataType: 'json',
                success: function (json) {
                    if (!json.hasError) {
                        $.fancybox.open('<div class="my-wishlist__alert my-wishlist__alert--success">' + json.msg + '</div>');
    
                    } else {
                        $.fancybox.open('<div class="my-wishlist__alert my-wishlist__alert--danger">' + json.error + '</div>');
                    }
                }
            });
        }
    }


}