import $ from 'jquery';


export default class SlickSlider {
  init() {
    let _this = this;
    $('[data-slick]').not('.slick-initialized').each(function () {
      let self = $(this);
      // if (self.data('count') === 1) {
      //     return;
      // }

      if (self.data('responsive-height')) {
        _this.responsiveHeight(this);
      }

      self.slick({
        prevArrow: `<button type="button" class="slick-prev slick-arrow" aria-label="Prev"><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M25.7812 15C25.7812 15.1243 25.7319 15.2436 25.6439 15.3315C25.556 15.4194 25.4368 15.4688 25.3125 15.4688H5.81919L13.4564 23.1061C13.5012 23.1493 13.5369 23.2011 13.5615 23.2583C13.586 23.3154 13.599 23.377 13.5995 23.4392C13.6 23.5014 13.5882 23.5632 13.5646 23.6208C13.541 23.6784 13.5062 23.7307 13.4622 23.7747C13.4182 23.8187 13.3659 23.8535 13.3083 23.8771C13.2507 23.9007 13.1889 23.9125 13.1267 23.912C13.0645 23.9115 13.0029 23.8985 12.9458 23.874C12.8886 23.8494 12.8368 23.8137 12.7936 23.7689L4.3561 15.3314C4.31258 15.2879 4.27805 15.2362 4.25449 15.1794C4.23094 15.1225 4.21881 15.0616 4.21881 15C4.21881 14.9385 4.23094 14.8775 4.25449 14.8206C4.27805 14.7638 4.31258 14.7121 4.3561 14.6686L12.7936 6.2311C12.882 6.14571 13.0004 6.09847 13.1233 6.09954C13.2462 6.1006 13.3638 6.1499 13.4507 6.23681C13.5376 6.32372 13.5869 6.44129 13.588 6.56419C13.589 6.6871 13.5418 6.80551 13.4564 6.89391L5.81919 14.5313H25.3125C25.4368 14.5313 25.556 14.5806 25.6439 14.6686C25.7319 14.7565 25.7812 14.8757 25.7812 15Z" fill="black"/></svg></button>`,
        nextArrow: `<button type="button" class="slick-next slick-arrow" aria-label="Next"><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M25.6439 15.3314L17.2064 23.7689C17.118 23.8543 16.9996 23.9016 16.8767 23.9005C16.7538 23.8994 16.6362 23.8501 16.5493 23.7632C16.4624 23.6763 16.4131 23.5587 16.412 23.4358C16.411 23.3129 16.4582 23.1945 16.5436 23.1061L24.1808 15.4688H4.6875C4.56318 15.4688 4.44395 15.4194 4.35604 15.3315C4.26814 15.2436 4.21875 15.1243 4.21875 15C4.21875 14.8757 4.26814 14.7565 4.35604 14.6686C4.44395 14.5806 4.56318 14.5313 4.6875 14.5313H24.1808L16.5436 6.89391C16.4582 6.80551 16.411 6.6871 16.412 6.56419C16.4131 6.44129 16.4624 6.32372 16.5493 6.23681C16.6362 6.1499 16.7538 6.1006 16.8767 6.09954C16.9996 6.09847 17.118 6.14571 17.2064 6.2311L25.6439 14.6686C25.6874 14.7121 25.722 14.7638 25.7455 14.8206C25.7691 14.8775 25.7812 14.9385 25.7812 15C25.7812 15.0616 25.7691 15.1225 25.7455 15.1794C25.722 15.2362 25.6874 15.2879 25.6439 15.3314Z" fill="black"/></svg></button>`
      });


    });
  }

  responsiveHeight(el) {
    const slickData = JSON.parse(el.dataset.slick);
    if (slickData.asNavFor) {
      const { asNavFor } = slickData;

      const mainWrapper = document.querySelector(asNavFor);
      if (mainWrapper) {
        const { offsetHeight } = mainWrapper;
        let { children } = el

        children = [...children];

        const totalChildrenHeight = children.reduce((height, child) => {
          return height + parseInt(child.offsetHeight);
        }, 0);

        const slidesToShow = (offsetHeight / (totalChildrenHeight / children.length)).toFixed();
        slickData.slidesToShow = parseInt(slidesToShow);

        el.dataset.slick = JSON.stringify(slickData);
      }
    }
  }

}
